cmake_minimum_required(VERSION 3.1)

# указать путь до модулей
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake_modules/")
#   ^переменная       ^новое значение (старое <пробел> добавить в конец)

# документация (http://habrahabr.ru/post/133512/)
MACRO(CONFIGURE_DOXYGEN_FILE DOXYGEN_CONFIG_FILE FILE_NAME_SUFFIX)
 IF(EXISTS ${PROJECT_SOURCE_DIR}/${DOXYGEN_CONFIG_FILE})
        FILE(REMOVE ${CMAKE_CURRENT_BINARY_DIR}/doxy-${FILE_NAME_SUFFIX}.conf)
        FILE(READ ${PROJECT_SOURCE_DIR}/${DOXYGEN_CONFIG_FILE} DOXYFILE_CONTENTS)
         STRING(REGEX REPLACE ";" "\\\\;" DOXYFILE_CONTENTS "${DOXYFILE_CONTENTS}")
         STRING(REGEX REPLACE "\n" ";" DOXYFILE_LINES "${DOXYFILE_CONTENTS}")
         LIST(LENGTH DOXYFILE_LINES ROW)
         MATH(EXPR ROW "${ROW} - 1")
         FOREACH(I RANGE ${ROW})
                LIST(GET DOXYFILE_LINES ${I} LINE)
                IF(LINE STRGREATER "")
                 STRING(REGEX MATCH "^[a-zA-Z]([^ ])+" DOXY_PARAM ${LINE})
                 IF(DEFINED DOXY_${DOXY_PARAM})
                        STRING(REGEX REPLACE "=([^\n])+" "= ${DOXY_${DOXY_PARAM}}" LINE ${LINE})
                 ENDIF(DEFINED DOXY_${DOXY_PARAM})
                ENDIF()
                FILE(APPEND ${CMAKE_CURRENT_BINARY_DIR}/doxy-${FILE_NAME_SUFFIX}.conf "${LINE}\n")
         ENDFOREACH()
 ELSE()
        MESSAGE(SEND_ERROR "Doxygen configuration file '${DOXYGEN_CONFIG_FILE}' not found. Documentation will not be generated")
 ENDIF()
ENDMACRO(CONFIGURE_DOXYGEN_FILE)


MACRO(ADD_DOCUMENTATION TARGET DOXYGEN_CONFIG_FILE)
 FIND_PACKAGE(Doxygen)
 IF(DOXYGEN_FOUND)
        CONFIGURE_DOXYGEN_FILE(${DOXYGEN_CONFIG_FILE} ${TARGET})
        ADD_CUSTOM_TARGET(${TARGET} COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/doxy-${TARGET}.conf)
 ELSE(DOXYGEN_FOUND)
        MESSAGE(STATUS "Doxygen not found. Documentation will not be generated")
 ENDIF(DOXYGEN_FOUND)
ENDMACRO(ADD_DOCUMENTATION)

#название проекта
project(GST)

#найти Qt, при этом переменные, связанные с ним обретут значения
find_package( Qt4 REQUIRED )
# найти libsettingscmdline аналогично
find_package( SETTINGSCMDLINE REQUIRED )
find_package( MYLOG REQUIRED )
find_package( MODBUS REQUIRED )
find_package( QMODBUS REQUIRED )
find_package( QWT REQUIRED )

#инклюды для сборки (-I<путь>)
include_directories (include
    ${QT_QTCORE_INCLUDE_DIR}
    ${QT_QTGUI_INCLUDE_DIR}
    ${QT_INCLUDE_DIR}
    ${SETTINGSCMDLINE_INCLUDE_DIR}
    ${MYLOG_INCLUDE_DIR}
    ${MODBUS_INCLUDE_DIR}
    ${QMODBUS_INCLUDE_DIR}
    ${QWT_INCLUDE_DIR}
    ${SETTINGSCMDLINE_INCLUDE_DIR}/..
    ${MYLOG_INCLUDE_DIR}/..
    ${MODBUS_INCLUDE_DIR}/..
    ${QMODBUS_INCLUDE_DIR}/..
    ${QWT_INCLUDE_DIR}/..
    src/include
    )

include(${QT_USE_FILE})

#список хедеров проекта (не Q_OBJECT'ов!)
set(${PROJECT_NAME}_HDRS
    src/include/serialdeviceenumerator.h
    src/include/stocktimescaledraw.h
    src/include/interpolation.h
    src/include/ap.h
    src/include/alglibinternal.h
    src/include/alglibmisc.h
    src/include/linalg.h
    src/include/solvers.h
    src/include/optimization.h
    src/include/specialfunctions.h
    src/include/integration.h
    src/include/slidemiddlefilter.h
    src/include/readexistingdatafromfile.h
    )

#список сырцов
set(${PROJECT_NAME}_SRCS
    src/main.cpp
    src/Settings.cpp
    src/gstversiondeterminator.cpp
    src/gstsystem.cpp
    src/gst.cpp
    src/DB00.cpp
    src/klapanswitcher.cpp
    src/mainform.cpp
    src/gstgraph.cpp
    src/stocktimescaledraw.cpp
    src/preasurestate.cpp
    src/stateprocessor.cpp
    src/optionswindow.cpp
    src/interpolation.cpp
    src/ap.cpp
    src/alglibinternal.cpp
    src/alglibmisc.cpp
    src/linalg.cpp
    src/solvers.cpp
    src/optimization.cpp
    src/specialfunctions.cpp
    src/integration.cpp
    src/inputlimits.cpp
    src/slidemiddlefilter.cpp
    src/readexistingdatafromfile.cpp
    )

#список хедеров Q_OBJECT'ов
set(${PROJECT_NAME}_MOCS
    src/include/Settings.h
    src/include/gstversiondeterminator.h
    src/include/gstsystem.h
    src/include/gst.h
    src/include/DB00.h
    src/include/klapanswitcher.h
    src/include/mainform.h
    src/include/gstgraph.h
    src/include/preasurestate.h
    src/include/stateprocessor.h
    src/include/optionswindow.h
    src/include/inputlimits.h
    )

#ресурсы
set(${PROJECT_NAME}_QRC
    res/res.qrc
    )
	
#команда выполнить moc для Q_OBJECT'ов
QT4_WRAP_CPP(MOCS ${${PROJECT_NAME}_MOCS})

#генерация ресурсов
QT4_ADD_RESOURCES(RCC ${${PROJECT_NAME}_QRC})

#собрать исполняемый файл с имянем ${PROJECT_NAME} из сырцов ${${PROJECT_NAME}_SRCS}
#и moc-ов ${MOCS}
add_executable(${PROJECT_NAME}
    ${${PROJECT_NAME}_SRCS}
    ${MOCS}
    ${RCC}
    )

if(WIN32)
SET(ADD_LIBS	setupapi)
elseif(UNIX)
find_package( UDEV REQUIRED )
SET(ADD_LIBS	${UDEV_LIBRARIES})
endif()

# генерация документации
SET(DOXY_OUTPUT_LANGUAGE "Russian")
SET(DOXY_INPUT ${PROJECT_SOURCE_DIR})

SET(DOXY_ENABLED_SECTIONS "developer_sec")
SET(DOXY_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/doc")
ADD_DOCUMENTATION(developer_doc Doxyfile)
ADD_CUSTOM_TARGET(doc DEPENDS developer_doc)

#либы, требующиеся для сборки цели
target_link_libraries(${PROJECT_NAME}
        ${CMAKE_SOURCE_DIR}/libqserialdevice.a
        ${ADD_LIBS}
        ${QT_QTCORE_LIBRARY}
        ${QT_QTGUI_LIBRARY}
        ${QT_LIBRARIES}
        ${SETTINGSCMDLINE_LIBRARIES}
        ${MYLOG_LIBRARIES}
        ${MODBUS_LIBRARIES}
        ${QMODBUS_LIBRARIES}
        ${QWT_LIBRARIES}
        )

#install
INSTALL(TARGETS ${PROJECT_NAME}
    RUNTIME DESTINATION bin
    )

########### Add uninstall target ###############
CONFIGURE_FILE(
  "${CMAKE_CURRENT_SOURCE_DIR}/cmake_modules/cmake_uninstall.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/cmake_modules/cmake_uninstall.cmake"
  IMMEDIATE @ONLY)
ADD_CUSTOM_TARGET(uninstall_${PROJECT_NAME}
  "${CMAKE_COMMAND}" -P "${CMAKE_CURRENT_BINARY_DIR}/cmake_modules/cmake_uninstall.cmake")

################# install path ###############
IF(MINGW)
  SET(CHICKEN_INSTALL_PREFIX "/usr/local" CACHE PATH "Mingw  default install path")
ELSE(MINGW)
  SET(CHICKEN_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX} CACHE PATH "default install path")
ENDIF(MINGW)
SET(CMAKE_INSTALL_PREFIX ${CHICKEN_INSTALL_PREFIX} CACHE INTERNAL "")
