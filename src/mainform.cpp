#include <math.h>

#include <QAction>
#include <QIcon>
#include <QMenu>
#include <QPushButton>
#include <QMenuBar>
#include <QActionGroup>
#include <QTabWidget>
#include <QApplication>
#include <QDesktopWidget>
#include <QStyle>
#include <QTime>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QDoubleSpinBox>
#include <QGridLayout>
#include <QLabel>
#include <QProgressBar>
#include <QMessageBox>
#include <QToolBar>
#include <QFileDialog>
#include <QTextCodec>
#include <QInputDialog>

#include <mylog/mylog.h>
#include <qmodbus/common.h>
#include <qmodbus/Sleeper.h>

#include "gst.h"
#include "gstsystem.h"
#include "DB00.h"
#include "klapanswitcher.h"
#include "gstgraph.h"
#include "preasurestate.h"
#include "optionswindow.h"
#include "readexistingdatafromfile.h"

#include "mainform.h"

static const char* iconNames[] =
{
    ":/res/atm.png", ":/res/pdown.png", ":/res/Pup.png"
};

static const char* klapansDesc[] =
{
    "Атмосферный клапан", "Клапан откачки", "Клапан натекания"
};

static const char* settingsKlapansNames[] =
{
    "Atmosphere", "Vacuum", "Preasure"
};

mainform::mainform(GSTSystem *gstSystem, QWidget *parent) :
    QMainWindow(parent)
{
    this->gstSystem = gstSystem;
    tabedWindow = NULL;
    blockAddMeasures = true;

    finishPreasureDownDialog = new QMessageBox(QMessageBox::Question, trUtf8("Сброс давления"),
                                               trUtf8("Давление в камере будет сброшено через 10 сек."),
                                               QMessageBox::Apply | QMessageBox::Cancel, this);
    finishPreasureDownDialog->setModal(true);
    timerPreasureDown.setInterval(1000);
    connect(&timerPreasureDown, SIGNAL(timeout()), this, SLOT(timerPreasureDownSlot()));

    OpenExistingGraph = new QAction(QIcon(":/res/folder-open.png"), trUtf8("Открыть..."), this);
    QMenu *pMenu = menuBar()->addMenu(trUtf8("Файл"));
    pMenu->addAction(OpenExistingGraph);
    connect(OpenExistingGraph, SIGNAL(triggered()), this, SLOT(OpenExistingGraphSlot()));

    pMenu = menuBar()->addMenu(trUtf8("Датчик"));
    QActionGroup* pAGroup = new QActionGroup(this);
    QAction* pAction;
    int i;
    for (i = 0; i < gstSystem->getSensors().size(); ++i)
    {
        uint8_t sensorAddr = gstSystem->getSensors().at(i)->getAddres();
        pAction = new QAction(trUtf8("Датчик 0x%1").arg(sensorAddr, 2, 16, QChar('0')), this);
        pAction->setData(sensorAddr);
        pAction->setCheckable(true);
        pAGroup->addAction(pAction);
        pMenu->addAction(pAction);
        SensorsList.append(pAction);
        connect(pAction, SIGNAL(triggered(bool)), this, SLOT(selectActiveSensor(bool)));
    }
    SensorsList.at(0/*TODO*/)->setChecked(true);
    gstSystem->setActiveSensor(SensorsList.at(0 /*TODO*/)->data().toUInt());

    if (!gstSystem->getKlapans().isEmpty())
    {
        pMenu = menuBar()->addMenu(trUtf8("Клапаны"));
        pAGroup = new QActionGroup(this);
        for (i = 0; i < gstSystem->getKlapans().size(); ++i)
        {
            uint8_t klapanAddr = gstSystem->getKlapans().at(i)->getAddres();
            pAction = new QAction(trUtf8("Контроллер клапанов 0x%1").arg(klapanAddr, 2, 16, QChar('0')), this);
            pAction->setData(klapanAddr);
            pAction->setCheckable(true);
            pAGroup->addAction(pAction);
            pMenu->addAction(pAction);
            KlapansList.append(pAction);
            connect(pAction, SIGNAL(triggered(bool)), this, SLOT(selectActiveKlapan(bool)));
        }
        KlapansList.at(0/*TODO*/)->setChecked(true);
        gstSystem->setActiveKlapanCtrl(KlapansList.at(0 /*TODO*/)->data().toUInt());
    }

    for(i = 0; i < gstSystem->getGST().size(); ++i)
    {
        GSTGraph* graph = new GSTGraph(NULL);
        connect(this, SIGNAL(settingsChanged()), graph, SLOT(AcceptNewSettings()));
        graphs.append(graph);
    }
    linearReadgrp = new QGroupBox(trUtf8("Чтение выходных значений"));
    LinearReadBtn = new QPushButton(trUtf8("Начать"));
    LinearReadBtn->setCheckable(true);
    linearReadgrp->setLayout(new QVBoxLayout);
    linearReadgrp->layout()->addWidget(LinearReadBtn);


    MeasureRK = new QGroupBox(trUtf8("Измерение Rk"));

    RkMeasureBth = new QPushButton(trUtf8("Начать"));
    RkMeasureBth->setCheckable(true);
    startPreasure = new QDoubleSpinBox;
    startPreasure->setMinimum(0);
    startPreasure->setMaximum(MAX_PREASURE);
    startPreasure->setValue(SettingsCmdLine::settings->value("MeasureRk/StartPreasure", DEFAULT_START_PREASURE).toInt());
    endPreasure = new QDoubleSpinBox;
    endPreasure->setMinimum(0);
    endPreasure->setMaximum(MAX_PREASURE);
    endPreasure->setValue(SettingsCmdLine::settings->value("MeasureRk/EndPreasure", DEFAULT_END_PREASURE).toInt());
    QGridLayout* gridLayout = new QGridLayout;
    QString pU = SettingsCmdLine::settings->value("Measure/PreasureUnitsName").toString();
    gridLayout->addWidget(new QLabel(trUtf8("Начальное давление [%1]").arg(pU)), 0, 0);
    gridLayout->addWidget(startPreasure, 0, 1);
    gridLayout->addWidget(new QLabel(trUtf8("Конечное давление [%1]").arg(pU)), 1, 0);
    gridLayout->addWidget(endPreasure);

    progressDesc = new QLabel;
    Progress = new QProgressBar;
    Progress->setMaximumWidth(190); // костыль
    Progress->setMinimum(0);
    Progress->setMaximum(100);
    Progress->setValue(0);
    QVBoxLayout* MeasureRKLayout = new QVBoxLayout;
    MeasureRKLayout->addWidget(RkMeasureBth);
    MeasureRKLayout->addLayout(gridLayout);
    MeasureRKLayout->addWidget(progressDesc);
    MeasureRKLayout->addWidget(Progress);
    MeasureRK->setLayout(MeasureRKLayout);

    QWidget* centralWgt = new QWidget;
    QVBoxLayout *rootLayout = new QVBoxLayout;
    rootLayout->addWidget(linearReadgrp);
    rootLayout->addWidget(MeasureRK);

    centralWgt->setLayout(rootLayout);

    setCentralWidget(centralWgt);

    connect(LinearReadBtn, SIGNAL(clicked(bool)), this, SLOT(StartLinearRead(bool)));
    connect(RkMeasureBth, SIGNAL(clicked(bool)), this, SLOT(StartMeasureRk(bool)));
    connect(&timer, SIGNAL(timeout()), this, SLOT(timerSlot()));

    preasureState = new PreasureState(this);
    preasureState->SetVacuum(10);
    preasureState->SetMaxPreasure(747 * 16);
    preasureState->setFilterLen(10);

    preasureStateProcessor = new StateProcessor(this);
    connect(preasureStateProcessor, SIGNAL(progress(QString,int)),
            this, SLOT(UpdateProgress(QString,int)));
    connect(preasureStateProcessor, SIGNAL(ValveRequest(QString,StateProcessor::valveReq)),
            this, SLOT(ProcessValve(QString,StateProcessor::valveReq)));
    connect(preasureStateProcessor, SIGNAL(SwitchDisplayModeReq(bool)),
            this, SLOT(SwitchDisplayMode(bool)));
    connect(preasureStateProcessor, SIGNAL(Interrupted()), this,
            SLOT(MeasureRKInterrupted()));
    connect(preasureStateProcessor, SIGNAL(MeasureFinished()), this,
            SLOT(measureFinish()));

    refreshSettings();

    AOptions = new QAction(QIcon(":/res/Tools1.png"), trUtf8("Настройка"), this);
    connect(AOptions, SIGNAL(triggered()), this, SLOT(CallOptionsWindow()));

    menuBar()->addMenu(trUtf8("Настройки"))->addAction(AOptions);
    QToolBar *toolbar = new QToolBar();
    toolbar->addAction(OpenExistingGraph);
    toolbar->addSeparator();
    toolbar->addAction(AOptions);
    toolbar->addSeparator();

    if (!KlapansList.isEmpty())
    {
        for (int i = 0; i < 3; ++i)
        {
            QAction* act = new QAction(QIcon(iconNames[i]), trUtf8(klapansDesc[i]), this);
            act->setCheckable(true);
            toolbar->addAction(act);
            connect(act, SIGNAL(triggered(bool)), this, SLOT(manualKlapanControlSlot(bool)));
            klapansButtons.append(act);
        }
    }
    addToolBar(Qt::TopToolBarArea, toolbar);

    timer.setInterval(SettingsCmdLine::settings->value("Measure/UpdateInterval_ms").toUInt());
}

mainform::~mainform()
{
    (*SettingsCmdLine::settings)["MeasureRk/StartPreasure"] = startPreasure->value();
    (*SettingsCmdLine::settings)["MeasureRk/EndPreasure"] = endPreasure->value();
    if (tabedWindow != NULL)
    {
        __DELETE(tabedWindow);
    }
    else
    {
        foreach (GSTGraph* graph, graphs)
            __DELETE(graph);
    }
}

void mainform::showEvent(QShowEvent *_e)
{
    showGstGraphs(SettingsCmdLine::settings->value("Interface/layout").toString() == trUtf8("tabs"));
    QMainWindow::showEvent(_e);
}

void mainform::closeEvent(QCloseEvent *_e)
{
    qApp->exit(0);
}

void mainform::selectActiveSensor(bool trigger)
{
    if (trigger)
    {
        uint8_t selectedActiveSensorAddr = static_cast<QAction*>(sender())->data().toUInt();
        if (selectedActiveSensorAddr != 0)
        {
            LOG_INFO(trUtf8("0x%1 is now active sensor").arg(selectedActiveSensorAddr, 2, 16, QChar('0')));
            gstSystem->setActiveSensor(selectedActiveSensorAddr);
        }
    }
}

void mainform::selectActiveKlapan(bool trigger)
{
    if (trigger)
    {
        uint8_t selectedActiveKlapanAddr = static_cast<QAction*>(sender())->data().toUInt();
        if (selectedActiveKlapanAddr != 0)
        {
            LOG_INFO(trUtf8("0x%1 is now active klapan").arg(selectedActiveKlapanAddr, 2, 16, QChar('0')));
            gstSystem->setActiveKlapanCtrl(selectedActiveKlapanAddr);
        }
    }
}

void mainform::StartLinearRead(bool startStop)
{
    MeasureRK->setEnabled(!startStop);
    blockAddMeasures = !startStop;
    if (startStop)
    {
        // старт
        LOG_INFO("Starting linear reading");
        refreshSettings();
        foreach(GSTGraph *graph, graphs)
            graph->setMode(GSTGraph::Rk_P_F_OF_T);
        LinearReadBtn->setText(trUtf8("Остановить"));
        timer.start();
    }
    else
    {
        //стоп
        LOG_INFO("Stopping linear reading");
        LinearReadBtn->setText(trUtf8("Начать"));
        timer.stop();
    }
}

void mainform::StartMeasureRk(bool startstop)
{
    // Сохранение
    if (startstop)
    {
        bool unsaved = false;
        foreach(GSTGraph *g, graphs)
        {
            if (g->hasUnsavedData())
            {
                unsaved = true;
                break;
            }
        }

        if (unsaved)
        {
            QMessageBox *saveAlert = new QMessageBox(QMessageBox::Warning, trUtf8("Подтверждеине сброса"),
                                                     trUtf8("Имеются несохраненные данные, сбросить?"),
                                                     QMessageBox::Apply | QMessageBox::Cancel, this);

            saveAlert->setModal(true);
            saveAlert->exec();
            QMessageBox::StandardButton res = (QMessageBox::StandardButton)saveAlert->result();
            delete saveAlert;
            if (res == QMessageBox::Cancel)
            {
                RkMeasureBth->setChecked(false);
                return;
            }
            else
                foreach(GSTGraph *g, graphs)
                    g->resetUnsaved();

        }
    }

    if (startPreasure->value() == endPreasure->value())
    {
        QMessageBox::critical(this, trUtf8("Ошибка"), trUtf8("Начальное и конечное давлеине должны различаться"));
        RkMeasureBth->setChecked(false);
        return;
    }
    if ((startPreasure->value() > endPreasure->value()) &&
            (startstop) &&
            (!SettingsCmdLine::settings->value("Measure/EnablePreasureHiToLo").toBool()))
    {
        QMessageBox::critical(this, trUtf8("Запрет"), trUtf8("Запрошеный режим работы от высокого к низкому запрещен,\nесли вы хотите его использовать, перейдите в настройки и разрешите его."));
        RkMeasureBth->setChecked(false);
        return;
    }
    linearReadgrp->setEnabled(!startstop);
    RkMeasureBth->setChecked(startstop);
    blockAddMeasures = !startstop;
    blockKlapansActions(startstop);
    if (startstop)
    {
        LOG_INFO("Start measuring Rk");
        refreshSettings();
        RkMeasureBth->setText(trUtf8("Прервать"));
        preasureStateProcessor->setWorkRange(startPreasure->value(), endPreasure->value());
        preasureState->reset();
        preasureStateProcessor->Start();
        timer.start();
    }
    else
    {
        timer.stop();
        preasureStateProcessor->Stop();
        if (!gstSystem->getKlapans().isEmpty())
            gstSystem->closeAllKlapans();
        RkMeasureBth->setText(trUtf8("Начать"));
        RkMeasureBth->setChecked(false);
        LOG_INFO("Measuring Rk stopped");
    }
}

void mainform::timerSlot()
{
    if (blockAddMeasures)
        return;
    QTime timestamp = QTime::currentTime();
    bool ok;
    float preasure = gstSystem->getPreasure(&ok);
    if (ok)
    {
        float filtredP = preasureState->addValue(preasure);
        preasureStateProcessor->UpdateState(preasureState->getState(), filtredP);

        GSTSystem::GSTData gstdata[3];
        uint8_t gstOk = gstSystem->getAllGSTData(gstdata);
        if (gstOk)
        {
            for (int i = 0; i < graphs.size(); ++i)
                if ((1 << i) & gstOk)
                {
                    if (blockAddMeasures)
                        return;
                    //graphs.at(i)->addData(preasure, gstdata[i].Rk, gstdata[i].F, timestamp);
                    graphs.at(i)->addData(filtredP, gstdata[i].Rk, gstdata[i].F, timestamp);
                }
                else
                    LOG_WARNING(trUtf8("Measure [%1] dropped for GST %2").arg(timestamp.toString()).arg(i));
        }
        else
            LOG_WARNING(trUtf8("Measure [%1] dropped: All GST").arg(timestamp.toString()));
    }
    else
        preasureState->addValue(preasureState->last()); // дублируем последнее дначение, чтобы не было Л-образных выбрасов (наверное)
        LOG_WARNING(trUtf8("Measure [%1] dropped: sensor").arg(timestamp.toString()));
}

void mainform::UpdateProgress(const QString &desc, int present)
{
    Progress->setValue(present);
    progressDesc->setText(desc);
}

void mainform::ProcessValve(const QString &desc, StateProcessor::valveReq req)
{
    int i = KLAPAN_EXEC_OPERATION_TRYS;
    QString name("Klapans/");
    switch (req)
    {
    case StateProcessor::CLOSE_ALL:
        while(i--)
            if (gstSystem->closeAllKlapans())
            {
                foreach(QAction* act, klapansButtons)
                {
                    act->blockSignals(true);
                    act->setChecked(false);
                    act->blockSignals(false);
                }
                return;
            }
        if (QMessageBox::Cancel == QMessageBox::question(
                    this, trUtf8("Запрос"), desc, QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Ok))
            MeasureRKInterrupted(); //стоп
        break;
    case StateProcessor::OPEN_VACUUM:
        while(i--)
            if (gstSystem->switchKlapan(SettingsCmdLine::settings->value(
                                            name + trUtf8(settingsKlapansNames[DEFAULT_VACUUM_KLAPAN]),
                                            DEFAULT_VACUUM_KLAPAN).toInt(), true))
            {
                QAction* vacKlapanAction = getKlapanAction(DEFAULT_VACUUM_KLAPAN);
                if (vacKlapanAction)
                {
                    vacKlapanAction->blockSignals(true);
                    vacKlapanAction->setChecked(true);
                    vacKlapanAction->blockSignals(false);
                }
                return;
            }
        if (QMessageBox::Cancel == QMessageBox::question(
                    this, trUtf8("Запрос"), desc, QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Ok))
            MeasureRKInterrupted(); //стоп
        break;
    case StateProcessor::OPEN_ATM:
        while(i--)
            if (gstSystem->switchKlapan(SettingsCmdLine::settings->value(
                                            name + trUtf8(settingsKlapansNames[DEFAULT_ATM_KLAPAN]),
                                            DEFAULT_ATM_KLAPAN).toInt(), true))
            {
                QAction* atmKlapanAction = getKlapanAction(DEFAULT_ATM_KLAPAN);
                if (atmKlapanAction)
                {
                    atmKlapanAction->blockSignals(true);
                    atmKlapanAction->setChecked(true);
                    atmKlapanAction->blockSignals(false);
                }
                return;
            }
        if (QMessageBox::Cancel == QMessageBox::question(
                    this, trUtf8("Запрос"), desc, QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Ok))
            MeasureRKInterrupted(); //стоп
        break;
    case StateProcessor::OPEN_PREASURE:
        while(i--)
            if (gstSystem->switchKlapan(SettingsCmdLine::settings->value(
                                            name + trUtf8(settingsKlapansNames[DEFAULT_PREASURE_KLAPAN]),
                                            DEFAULT_PREASURE_KLAPAN).toInt(), true))
            {
                QAction* preasureKlapanAction = getKlapanAction(DEFAULT_PREASURE_KLAPAN);
                if (preasureKlapanAction)
                {
                    preasureKlapanAction->blockSignals(true);
                    preasureKlapanAction->setChecked(true);
                    preasureKlapanAction->blockSignals(false);
                }
                return;
            }
        if (QMessageBox::Cancel == QMessageBox::question(
                    this, trUtf8("Запрос"), desc, QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Ok))
            MeasureRKInterrupted(); //стоп
        break;
    }
}

void mainform::SwitchDisplayMode(bool _P)
{
    foreach (GSTGraph* graph, graphs)
        graph->setMode(_P ? (GSTGraph::RK_F_OF_P) : (GSTGraph::Rk_P_F_OF_T));
}

void mainform::measureFinish()
{
    StartMeasureRk(false);
    foreach(GSTGraph* graph, graphs)
    {
        graph->BuildRkAccuracyPipe();
        graph->BuildFErrorCurve();
    }

    if (!gstSystem->getKlapans().isEmpty())
    {
        counterPreasureDown = 10;
        timerPreasureDown.start();
        if (finishPreasureDownDialog->exec() != QMessageBox::Rejected)
        {
            LOG_INFO(trUtf8("Preasure down accepted"));
            const int trys = 5;
            int t = trys;
            QString name("Klapans/");
            while (!gstSystem->switchKlapan(SettingsCmdLine::settings->value(
                                                name + trUtf8(settingsKlapansNames[DEFAULT_ATM_KLAPAN]),
                                                DEFAULT_ATM_KLAPAN).toInt(), true))
            {
                if (t--)
                {
                    Sleeper::sleep_ms(10);
                    LOG_WARNING("Failed to request preasure down");
                    QApplication::processEvents();
                }
                else
                {
                    LOG_ERROR("Klapan controller is not ansver.");
                    goto __fin_measureFinish;
                }
            }

            float atm = SettingsCmdLine::settings->value("Measure/AtmospherePreasure").toFloat();
            float atm_tolerance = SettingsCmdLine::settings->value("Measure/AtmosphereTolerancee").toFloat();
            unsigned int measureInterval = SettingsCmdLine::settings->value("Measure/UpdateInterval_ms").toUInt();

            while (true)
            {
                bool ok;
                float P = gstSystem->getPreasure(&ok);
                if (ok &&
                        (P > atm - atm_tolerance) &&
                        (P < atm + atm_tolerance))
                {
                    t = trys;
                    while (!gstSystem->closeAllKlapans())
                    {
                        if (t--)
                        {
                            Sleeper::sleep_ms(10);
                            LOG_WARNING("Failed to request close all klapans");
                            QApplication::processEvents();
                        }
                        else
                        {
                            LOG_ERROR("Klapan controller is not ansver.");
                            break;
                        }
                    }
                    break;
                }
                else
                {
                    Sleeper::sleep_ms(measureInterval);
                    QApplication::processEvents();
                }
            }
        }
    }

__fin_measureFinish:
    int i = 0;
    foreach(GSTGraph* graph, graphs)
    {
        graph->setGraphTitle(QInputDialog::getText(graph, trUtf8("Укажите название"),
                                                   trUtf8("Название резонатора, подключенного к ГСТ 0x%1").arg(
                                                       gstSystem->getGST().at(i)->getAddres(), 2, 16, QChar('0'))));
        ++i;
    }
}

void mainform::MeasureRKInterrupted()
{
    StartMeasureRk(false);
}

void mainform::manualKlapanControlSlot(bool trigger)
{
    int trys = KLAPAN_EXEC_OPERATION_TRYS;
    QAction* klapanAction = (QAction*)sender(); // сама кнопчка
    int nKlapan = klapansButtons.indexOf(klapanAction); // номер (для определения назначения)

    // выключить все
    LOG_DEBUG("All klapans off");
    while (true)
        if (gstSystem->closeAllKlapans())
            break;
        else
            if (--trys <= 0)
            {
                LOG_ERROR("Failed to execute close all request");
                return;
            }

    foreach(QAction* act, klapansButtons)
    {
        act->blockSignals(true);
        act->setChecked(false);
        act->blockSignals(false);
    }

    if (trigger)
    {
        QString name("Klapans/");

        // при попытке открыть вакуум при давлении в камере больше атмосферного открыть клапан атмосферы вместо него
        if (nKlapan == 1)
        {
            bool ok;
            float P;
            trys = KLAPAN_EXEC_OPERATION_TRYS;
            while (true)
            {
                P = gstSystem->getPreasure(&ok);
                if (!ok)
                {
                    if (trys-- == 0)
                    {
                        LOG_ERROR("Cannot process vacuum request, failed to get current preasure");
                        return;
                    }
                }
                else
                    break;
            }
            if (P > SettingsCmdLine::settings->value("Measure/AtmospherePreasure").toDouble() +
                    SettingsCmdLine::settings->value("Measure/AtmosphereTolerancee").toDouble())
            {
                LOG_WARNING("Current preasure more than atmosphere, opening atmosphere valve");
                nKlapan = 0;
            }
        }

        trys = KLAPAN_EXEC_OPERATION_TRYS;
        while (trys--)
            if (gstSystem->switchKlapan(SettingsCmdLine::settings->value(
                                            name + trUtf8(settingsKlapansNames[nKlapan]),
                                            nKlapan).toInt(), true))
            {
                LOG_INFO(trUtf8("%1 klapan opened.").arg(trUtf8(settingsKlapansNames[nKlapan])));
                klapansButtons.at(nKlapan)->blockSignals(true);
                klapansButtons.at(nKlapan)->setChecked(true);
                klapansButtons.at(nKlapan)->blockSignals(false);
                break;
            }
    }
}

void mainform::timerPreasureDownSlot()
{
    finishPreasureDownDialog->setText(trUtf8("Давление в камере будет сброшено через %1 сек.").arg(--counterPreasureDown));
    if (counterPreasureDown <= 0)
    {
        timerPreasureDown.stop();
        finishPreasureDownDialog->accept();
    }
}

void mainform::CallOptionsWindow()
{
    OptionsWindow optionsWindow;
    if (optionsWindow.exec() == QDialog::Accepted)
    {
        showGstGraphs(SettingsCmdLine::settings->value("Interface/layout").toString() == trUtf8("tabs"));
        preasureState->SetAtmosphere(SettingsCmdLine::settings->value("Measure/AtmospherePreasure").toDouble());
        preasureState->SetAtmosphereSigma(SettingsCmdLine::settings->value("Measure/AtmosphereTolerancee").toDouble());
        preasureState->SetStableMaxDelta(SettingsCmdLine::settings->value("Measure/MaxStableDelta").toDouble());
        timer.setInterval(SettingsCmdLine::settings->value("Measure/UpdateInterval_ms").toUInt());
        raise(); // преводит это окно на предний план
        emit settingsChanged();
    }
}

void mainform::OpenExistingGraphSlot()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    trUtf8("Открыть старое измерение"),
                                                    QDir::currentPath(),
                                                    trUtf8("CSV table (*.csv)"));

    GSTGraph *customGraph = ReadExistingDataFromFile::OpenExistingData(filename);
    if (customGraph != NULL)
    {
        customGraph->resize(WINDOW_DEF_H, WINDOW_DEF_W);
        customGraph->setGeometry(
                    QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, customGraph->size(),
                                        qApp->desktop()->availableGeometry()));
        connect(this, SIGNAL(settingsChanged()), customGraph, SLOT(AcceptNewSettings()));
        customGraph->show();
        raise();
    }
}

void mainform::showGstGraphs(bool isTabbed)
{
    if (isTabbed)
    { // вкладки
        if (tabedWindow == NULL)
        {
            foreach (GSTGraph* graph, graphs)
                graph->hide();

            tabedWindow = new QTabWidget(NULL); // Окно верхнего уровня.
            tabedWindow->setWindowTitle(trUtf8("Графики"));
            foreach (GSTGraph* graph, graphs)
            {
                tabedWindow->addTab(graph, trUtf8("ГСТ 0x%1").arg(
                                        gstSystem->getGST().at(graphs.indexOf(graph))->getAddres(), 2, 16, QChar('0')));
                //graph->setParent(this);
            }
            tabedWindow->setWindowFlags(Qt::Window
                                        | Qt::WindowMinimizeButtonHint
                                        | Qt::WindowMaximizeButtonHint
                                        | Qt::CustomizeWindowHint);
            tabedWindow->resize(WINDOW_DEF_H, WINDOW_DEF_W);
            tabedWindow->setGeometry(
                        QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, tabedWindow->size(),
                                            qApp->desktop()->availableGeometry()));
        }
        tabedWindow->show();
    }
    else
    { // отдельные окна
        if (tabedWindow != NULL)
        {
            foreach (GSTGraph* graph, graphs)
                graph->setParent(NULL);

            __DELETE(tabedWindow);
        }
        foreach (GSTGraph* graph, graphs)
        {
            graph->setWindowTitle(trUtf8("ГСТ 0x%1").arg(
                                      gstSystem->getGST().at(graphs.indexOf(graph))->getAddres(), 2, 16, QChar('0')));
            graph->setWindowFlags(Qt::Window
                                  | Qt::WindowMinimizeButtonHint
                                  | Qt::WindowMaximizeButtonHint
                                  | Qt::CustomizeWindowHint);
            graph->setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, graph->size(),
                                                   qApp->desktop()->availableGeometry()));
            graph->show();
        }
    }
}

QAction *mainform::getKlapanAction(int n)
{
    if ((!klapansButtons.isEmpty()) && klapansButtons.size() > n)
        return klapansButtons.at(n);
    else
        return NULL;
}

void mainform::blockKlapansActions(bool block)
{
    foreach(QAction* act, klapansButtons)
        act->setEnabled(!block);
}

void mainform::refreshSettings()
{
    double atm = SettingsCmdLine::settings->value("Measure/AtmospherePreasure").toDouble();
    preasureState->SetAtmosphere(atm);
    preasureState->SetAtmosphereSigma(SettingsCmdLine::settings->value("Measure/AtmosphereTolerancee").toDouble());
    preasureState->SetStableMaxDelta(SettingsCmdLine::settings->value("Measure/MaxStableDelta").toDouble());

    preasureStateProcessor->setAtmospherePreasure(atm);
}
