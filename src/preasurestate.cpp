#include <mylog/mylog.h>

#include "preasurestate.h"

PreasureState::PreasureState(QObject *parent) :
    QObject(parent)
{
    curentState = IDLE;
}

void PreasureState::SetMaxPreasure(float maxPreasure)
{
    MaxPreasure = maxPreasure;
}

void PreasureState::SetVacuum(float vacuum)
{
    VacuumPreasure = vacuum;
}

void PreasureState::SetAtmosphere(float AtmPreasure)
{
    AtmospherePreasure = AtmPreasure;
}

void PreasureState::SetAtmosphereSigma(float AtmSigma)
{
    AtmosphereSigma = AtmSigma;
}

void PreasureState::SetStableMaxDelta(float stableMaxDelta)
{
    this->StableMaxDelta = stableMaxDelta;
}

PreasureState::PreasureStates PreasureState::getState() const
{
    return curentState;
}

float PreasureState::addValue(float newVal)
{
    float res = SlideMiddleFilter::addValue(newVal);
    setState(DetectState(oldest(),
                         middle(),
                         newVal));
    return res;
}

void PreasureState::setState(PreasureState::PreasureStates newstate)
{
    if (curentState != newstate)
    {
        curentState = newstate;
        LOG_DEBUG(trUtf8("Changing preasure state: %1").arg(enPreasureStates2Text(newstate)));
        emit StateChanged(newstate);
    }
    else
        curentState = newstate;
}

QString PreasureState::enPreasureStates2Text(PreasureState::PreasureStates state)
{
    QString res;
    if (state & MAXIMAL)
        res.append("MAXIMAL");
    if (state & VACUUM)
        res.append(" | VACUUM");
    if (state & LESS_ATM)
        res.append(" | LESS_ATM");
    if (state & NEAR_ATM)
        res.append(" | NEAR_ATM");
    if (state & MORE_ATM)
        res.append(" | MORE_ATM");
    if (state & UPING)
        res.append(" | UPING");
    if (state & FALLING)
        res.append(" | FALLING");
    if (state & STABLE)
        res.append(" | STABLE");
    if (res.isEmpty())
        res = trUtf8("IDLE");
    return res;
}

PreasureState::PreasureStates PreasureState::DetectState(float oldest, float middle, float current)
{
    // атмосфера
    PreasureStates result = IDLE;
    if (getfillFilterCounter() < getfilterLen())
        return IDLE;
    if ((current < (AtmospherePreasure + AtmosphereSigma)) &&
            (current > (AtmospherePreasure - AtmosphereSigma)))
        result = static_cast<PreasureStates>(result | NEAR_ATM);
    else
    {
        if (current > (AtmospherePreasure + AtmosphereSigma))
            result = static_cast<PreasureStates>(result | MORE_ATM);
        if (current < (AtmospherePreasure - AtmosphereSigma))
            result = static_cast<PreasureStates>(result | LESS_ATM);
    }

    if (current <= VacuumPreasure)
        result = static_cast<PreasureStates>(result | VACUUM);

    if (current >= MaxPreasure)
        result = static_cast<PreasureStates>(result | MAXIMAL);

    if (middle - oldest > StableMaxDelta)
    {
        if (current - middle > StableMaxDelta)
            result = static_cast<PreasureStates>(result | UPING); // монотонное возрастание
    }
    else if (oldest - middle > StableMaxDelta)
    {
        if (middle - current > StableMaxDelta)
            result = static_cast<PreasureStates>(result | FALLING); // монотонное убывание
    }
    else
    { // ==
        if (current - middle > StableMaxDelta)
            result = static_cast<PreasureStates>(result | UPING); // стаганция перешла в возрастание
        else if (middle - current > StableMaxDelta)
            result = static_cast<PreasureStates>(result | FALLING); // стаганция перешла в убывание
        else
            result = static_cast<PreasureStates>(result | STABLE); // стагнация.

    }
    return result;
}
