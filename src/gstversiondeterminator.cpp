
#include <QPushButton>
#include <QLabel>
#include <QProgressBar>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTimer>
#include <QAbstractAnimation>
#include <QApplication>
#include <QMessageBox>

#include <qmodbus/ModbusEngine.h>
#include <qmodbus/ModbusRequest.h>
#include <qmodbus/common.h>
#include <qmodbus/Sleeper.h>
#include <mylog/mylog.h>
#include <modbus/modbus.h>

#include "SerialDeviceEnumerator.h"
#include "gstversiondeterminator.h"
#include "gstsystem.h"

#include "gst.h"
#include "DB00.h"
#include "klapanswitcher.h"

#define SERTIFIED_SENSOR_ID 0xDBA0

static QString fix_port_name(QString& port) {
    if (port.startsWith("COM")){
        return "\\\\.\\" + port;
    } else {
        return port;
    }
}

GSTVersionDeterminator::GSTVersionDeterminator(ModbusEngine *mb, QWidget *parent) :
    QDialog(parent)
{
    gstSystem = NULL;
    fCancel = false;
    this->mb = mb;

    Infostring = new QLabel();
    progressBar = new QProgressBar;
    progressBar->setMinimum(1);
    progressBar->setMaximum(254);
    cancelButton = new QPushButton(trUtf8("Прервать"));

    QHBoxLayout* progresLayout = new QHBoxLayout;
    progresLayout->addWidget(progressBar);
    progresLayout->addWidget(cancelButton);

    QVBoxLayout* rootLayout = new QVBoxLayout;
    rootLayout->addWidget(Infostring);
    rootLayout->addLayout(progresLayout);

    setLayout(rootLayout);

    connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
}

GSTSystem *GSTVersionDeterminator::getSystem() const
{
    return gstSystem;
}

void GSTVersionDeterminator::reject()
{
    fCancel = true;
    QDialog::reject();
}

void GSTVersionDeterminator::showEvent(QShowEvent *event)
{
    QTimer::singleShot(1000, this, SLOT(StartScan()));
    QDialog::showEvent(event);
}

uint16_t GSTVersionDeterminator::deviceIDAtAdress(uint8_t adress)
{
    Infostring->setText(trUtf8("Проверка связи с 0x%1").arg(adress, 2, 16, QChar('0')));
    ModbusRequest *req = ModbusRequest::BuildReadHoldingRequest(adress, DEVICES_ID_ADRESS);
    uint16_t result = 0;
    mb->SyncRequest(*req);
    if (req->getErrorrCode() == ModbusRequest::ERR_OK)
        result = req->getAnsverAsShort().at(0);
    QApplication::processEvents();
    req->deleteLater();
    return result;
}

void GSTVersionDeterminator::StartScan()
{
    LOG_INFO("Detecting avalable ports...");
    SerialDeviceEnumerator* enumerator = SerialDeviceEnumerator::instance();
    QStringList avalable = enumerator->devicesAvailable();

    foreach(QString str, avalable)
    {
        enumerator->setDeviceName(str);
        LOG_DEBUG(trUtf8(">>> info about: %1").arg(enumerator->name()));

        LOG_DEBUG(trUtf8("-> description  : %1").arg(enumerator->description()));
        LOG_DEBUG(trUtf8("-> driver       : %1").arg(enumerator->driver()));
        LOG_DEBUG(trUtf8("-> friendlyName : %1").arg(enumerator->friendlyName()));
        LOG_DEBUG(trUtf8("-> hardwareID   : %1").arg(enumerator->hardwareID().join(" ")));
        LOG_DEBUG(trUtf8("-> locationInfo : %1").arg(enumerator->locationInfo()));
        LOG_DEBUG(trUtf8("-> manufacturer : %1").arg(enumerator->manufacturer()));
        LOG_DEBUG(trUtf8("-> productID    : %1").arg(enumerator->productID()));
        LOG_DEBUG(trUtf8("-> service      : %1").arg(enumerator->service()));
        LOG_DEBUG(trUtf8("-> shortName    : %1").arg(enumerator->shortName()));
        LOG_DEBUG(trUtf8("-> subSystem    : %1").arg(enumerator->subSystem()));
        LOG_DEBUG(trUtf8("-> systemPath   : %1").arg(enumerator->systemPath()));
        LOG_DEBUG(trUtf8("-> vendorID     : %1").arg(enumerator->vendorID()));

        LOG_DEBUG(trUtf8("-> revision     : %1").arg(enumerator->revision()));
        LOG_DEBUG(trUtf8("-> bus          : %1").arg(enumerator->bus()));
        //
        LOG_DEBUG(trUtf8("-> is exists    : %1").arg(enumerator->isExists()));
        LOG_DEBUG(trUtf8("-> is busy      : %1").arg(enumerator->isBusy()));
    }

    foreach(QString portname, avalable)
    {
        enumerator->setDeviceName(portname);
        if ((enumerator->vendorID() != _FT_VID) || (enumerator->productID() != _FT_PID) || (enumerator->isBusy() == true))
        {
#ifdef WIN32
            // В зверь CD Вместо названия вендора такая строчка
            if (enumerator->vendorID() != WIN32_ZVERCD_KOSTIL &&
                // в WIN10 VID и PID пыстые, зато hardwareID содержит такую строку
                !enumerator->hardwareID().empty() && enumerator->hardwareID().first() != WIN32_ZVERCD_KOSTIL) {
                avalable.removeOne(portname);
            }
#else
             avalable.removeOne(portname);
#endif
        }
        else
            if (portname.at(0) != '/')
            {
                QString number = portname;
                number.remove(0, 3);
                if (number.toInt() > 9)
                {
                    avalable.removeOne(portname);
                    avalable.append("\\\\.\\" + portname);
                }
            }
    }
    enumerator->deleteLater();

    if (avalable.isEmpty())
    {
        QMessageBox::critical(this, trUtf8("Поддерживаемых устройств не найдено"),
                              trUtf8("Не найдено виртуальных последовательных портов FTDI.\nУбедитесь, что устройство подключено."));
        LOG_FATAL("No adapters found, exiting");
        reject();
        return;
    }
    LOG_INFO(trUtf8("Found ports: %1").arg(avalable.join(", ")));
    QString port = SettingsCmdLine::settings->value("LastSucess/Port").toString();
    if ((!port.isEmpty()) &&  (!SettingsCmdLine::settings->value("Global/ForceRescan").toBool()))
    {
        // В настройках есть информация об удачном запуске
        // Пробуем подтвердить
        LOG_INFO(trUtf8("Found previous sucess run: port %1").arg(port));
        setWindowTitle(trUtf8("Cоединение..."));
        (*SettingsCmdLine::settings)["Global/Port"] = port;
        mb->RestartConnection();
        mb->setTimeout(SettingsCmdLine::settings->value("Global/Timeout", 30).toInt());

        modbus_set_response_timeout(mb->getModbusContext(), 0,
                                    SettingsCmdLine::settings->value("Global/ResponseTimeout", DEFAULT_TIMEOUT).toULongLong());

        bool ok = true;
        progressBar->setMinimum(0);
        progressBar->setValue(0);
        progressBar->setMaximum(6);
        progressBar->setMaximumWidth(200); // костыль

        int i;
        uint8_t addr;
        gstSystem = new GSTSystem(mb, this);
        for (i = 0; i < 3; ++i)
        {
            progressBar->setValue(progressBar->value() + 1);
            addr = SettingsCmdLine::settings->value(trUtf8("LastSucess/GST%1Addr").arg(i)).toUInt();
            if (addr != 0)
                gstSystem->addGST(addr);
        }
        for (i = 0; i < 2; ++i)
        {
            progressBar->setValue(progressBar->value() + 1);
            addr = SettingsCmdLine::settings->value(trUtf8("LastSucess/Sensor%1Addr").arg(i)).toUInt();
            if (addr != 0)
                gstSystem->addSensor(addr);
        }
        progressBar->setValue(progressBar->value() + 1);
        addr = SettingsCmdLine::settings->value("LastSucess/KlapanCtrlAddr").toUInt();
        if (addr != 0)
            gstSystem->AddKlapans(addr);

        gstSystem->removeUnconnectedDevices();
        if (gstSystem->TestMinimalRequred())
        {
            gstSystem->SaveConfiguration();
            LOG_INFO("Connection confurmed.");
            accept();
            return;
        }
        else
        {
            LOG_WARNING("Prevues configuration not confurmed, redetecting");
            (*SettingsCmdLine::settings)["LastSucess/Port"] = "";
            __DELETE(gstSystem);
        }
    }

    // ищем все заново

    //(*SettingsCmdLine::settings)["Global/ForceRescan"] = false;
    progressBar->setMinimum(0);
    progressBar->setValue(0);
    progressBar->setMaximum(SCAN_MAX_ADRESS);

    modbus_set_response_timeout(mb->getModbusContext(), 0,
                                SettingsCmdLine::settings->value("Global/ResponseTimeout", DEFAULT_TIMEOUT).toULongLong());

    foreach(port, avalable)
    {
        LOG_INFO(trUtf8("Scaning port %1").arg(port));
        setWindowTitle(trUtf8("Поиск: %1").arg(port));
#ifdef WIN32
        (*SettingsCmdLine::settings)["Global/Port"] = fix_port_name(port);
#else
        (*SettingsCmdLine::settings)["Global/Port"] = port;
#endif

        mb->RestartConnection();
        mb->setTimeout(SettingsCmdLine::settings->value("Global/Timeout", 30).toInt());

        gstSystem = new GSTSystem(mb, this);
        for (uint8_t addr = 1; addr < SCAN_MAX_ADRESS; ++addr)
        {
            progressBar->setValue(addr);
            QApplication::processEvents();
            if (fCancel)
            {
                __DELETE(gstSystem);
                return; // отмена!
            }
            uint16_t foundID = deviceIDAtAdress(addr);
            switch (foundID)
            {
            case 0:
                break;
            case _DB00_DEVICE_ID:
                LOG_INFO(trUtf8("Found 0xDB00 sensor at adress 0x%1").arg(addr, 2, 16, QChar('0')));
                gstSystem->addSensor(addr);
                break;

            // Да, это другой датчик, но адрес давления одинаковый, поэтому просто оставим так.
            case SERTIFIED_SENSOR_ID:
                LOG_INFO(trUtf8("Found 0xDBA0 sensor at adress 0x%1").arg(addr, 2, 16, QChar('0')));
                gstSystem->addSensor(addr);
                break;

            case GST_ID:
                LOG_INFO(trUtf8("Found GST at adress 0x%1").arg(addr, 2, 16, QChar('0')));
                gstSystem->addGST(addr);
                break;
            case KLAPAN_CONTROLLER_ID:
                LOG_INFO(trUtf8("Found Klapan controller at adress 0x%1").arg(addr, 2, 16, QChar('0')));
                gstSystem->AddKlapans(addr);
                break;
            default:
                LOG_INFO(trUtf8("Found unsupported device ID: 0x%1, adress: %2").arg(foundID, 2, 16, QChar('0')).arg(addr));
            }
        }
        if (gstSystem->TestMinimalRequred())
        {
            LOG_INFO("Connection confurmed.");
            gstSystem->SaveConfiguration();
            accept();
            return;
        }
        else
        {
            __DELETE(gstSystem);
        }
    }
    if (gstSystem == NULL)
    {
        QMessageBox::critical(this, trUtf8("Совместимых конфигураций не обнаружено"),
                              trUtf8("Сканирование завершено - совместимых конфигураций не обнаружено\nпроверьте подключения устрйоств."));
        reject();
    }
}


