#include <QDoubleSpinBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QGridLayout>
#include <QMessageBox>

#include <settingscmdline/settingscmdline.h>

#include "inputlimits.h"

inputLimits::inputLimits(const int Min, const int Max, QWidget *parent) :
    QDialog(parent)
{
    QHBoxLayout *btnLayout = new QHBoxLayout;
    QPushButton *okbtn = new QPushButton(trUtf8("Ok"));
    QPushButton *cancelbtn = new QPushButton(trUtf8("Отмена"));

    connect(okbtn, SIGNAL(clicked()), this, SLOT(Validate()));
    connect(cancelbtn, SIGNAL(clicked()), this, SLOT(reject()));

    btnLayout->addWidget(okbtn);
    btnLayout->addWidget(cancelbtn);

    QGridLayout *grid = new QGridLayout;
    QString pU = SettingsCmdLine::settings->value("Measure/PreasureUnitsName").toString();
    grid->addWidget(new QLabel(trUtf8("Начало [%1]").arg(pU)), 0, 0);
    grid->addWidget(new QLabel(trUtf8("Конец [%1]").arg(pU)), 1, 0);

    sbFrom = new QDoubleSpinBox;
    sbFrom->setMinimum(Min);
    sbFrom->setMaximum(Max);
    sbFrom->setValue(Min);
    sbFrom->setSingleStep(0.1);
    sbTo = new QDoubleSpinBox;
    sbTo->setMinimum(Min);
    sbTo->setMaximum(Max);
    sbTo->setValue(Max);
    sbTo->setSingleStep(0.1);

    grid->addWidget(sbFrom, 0, 1);
    grid->addWidget(sbTo, 1, 1);

    QVBoxLayout* rootlayout = new QVBoxLayout;
    rootlayout->addLayout(grid);
    rootlayout->addLayout(btnLayout);
    setLayout(rootlayout);
    setWindowTitle(trUtf8("Фрагмент"));
}

float inputLimits::from() const
{
    return sbFrom->value();
}

float inputLimits::to() const
{
    return sbTo->value();
}

void inputLimits::Validate()
{
    if (sbFrom->value() < sbTo->value())
        accept();
    else
        QMessageBox::critical(this, trUtf8("Неверный диопазон"), trUtf8("Начальное давление больше конечного"));
}
