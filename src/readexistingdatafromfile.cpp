#include <QObject>
#include <QTextCodec>
#include <QFile>
#include <QList>
#include <QByteArray>

#include <mylog/mylog.h>

#include "gstgraph.h"

#include "readexistingdatafromfile.h"

GSTGraph *ReadExistingDataFromFile::OpenExistingData(const QString& filename)
{
    const QString dataerrStr = QObject::trUtf8("Broken format of input data (%1)");
    GSTGraph* res = NULL;
    if (filename.isEmpty())
        return NULL;
    QFile datafile(filename);
    if (!datafile.open(QIODevice::ReadOnly))
    {
        LOG_ERROR(QObject::trUtf8("Failed to open file %1").arg(filename));
    }
    else
    {
        QByteArray headTest;
        QList<QByteArray> split;
        QTextCodec *codec = QTextCodec::codecForName("Windows-1251"); // кодек, для перкодирования из CP1251

        // парсим файл
        headTest = datafile.readLine();
        if (headTest.isEmpty())
        {
            LOG_ERROR(QObject::trUtf8("File %1 is empty").arg(filename));
        }
        else
        {
            split = headTest.split(' ');
            if ((codec->toUnicode(split.at(0)) != QObject::trUtf8("Отчёт")) || (split.size() < 2))
            {
                LOG_ERROR(dataerrStr.arg("Head"));
            }
            else
            {
                QString dateStr = split.at(1);
                headTest = datafile.readLine();
                if (headTest.isEmpty())
                {
                    LOG_ERROR(dataerrStr.arg("Unexpected EOF"));
                }
                else
                {
                    if (codec->toUnicode(headTest) != QObject::trUtf8("Давление;Rk;F\n"))
                    {
                        LOG_ERROR(dataerrStr.arg("Cloumn names wrong"));
                    }
                    else
                    {
                        QVector<QPointF> Rk, F;
                        bool ok = true;
                        while(!datafile.atEnd())
                        {
                            split =  datafile.readLine().replace(',', '.').split(';');
                            if (split.size() != 3)
                            {
                                ok = false;
                                break;
                            }
                            else
                            {
                                bool r[3];
                                float _P = split.at(0).toFloat(&r[0]);
                                float _Rk = split.at(1).toFloat(&r[1]);
                                float _F = split[2].replace(" \n", QByteArray()).toFloat(&r[2]);
                                if (r[0] & r[1] & r[2])
                                {
                                    Rk.append(QPointF(_P, _Rk));
                                    F.append(QPoint(_P, _F));
                                }
                                else
                                {
                                    ok = false;
                                    break;
                                }
                            }

                        }
                        if (ok)
                        {
                            GSTGraph *customGraph = new GSTGraph;
                            customGraph->setAttribute(Qt::WA_DeleteOnClose);
                            customGraph->setWindowTitle(QObject::trUtf8("%1 [%2]").arg(filename).arg(dateStr));
                            customGraph->showCustomRk_P_graph(Rk, F);
                            customGraph->BuildRkAccuracyPipe();
                            customGraph->BuildFErrorCurve();

                            customGraph->setGraphTitle(filename.split("/").last().split(".").first());

                            res = customGraph;
                        }
                        else
                        {
                            LOG_ERROR(dataerrStr.arg("Parse Data error"));
                        }
                    }
                }
            }
        }
        datafile.close();
    }
    return res;
}
