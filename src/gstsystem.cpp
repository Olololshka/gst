#include <QApplication>

#include <modbus/modbus.h>
#include <qmodbus/common.h>
#include <qmodbus/Sleeper.h>
#include <qmodbus/ModbusEngine.h>
#include <settingscmdline/settingscmdline.h>
#include <mylog/mylog.h>

#include "gst.h"
#include "DB00.h"
#include "klapanswitcher.h"

#include "gstsystem.h"

GSTSystem::GSTSystem(ModbusEngine *mb, QObject *parent) :
    QObject(parent)
{
    this->mb = mb;
    ActiveKlapan = NULL;
    ActiveSensor = NULL;
    mb->setTryCloseonError(false);
}

void GSTSystem::addGST(const uint8_t addr)
{
    lGst.append(new GST(mb, addr, this));
}

void GSTSystem::addSensor(const uint8_t addr)
{
    lSensors.append(new DB00(mb, addr, this));
}

void GSTSystem::AddKlapans(const uint8_t addr)
{
    KlapanSwitcher* klapan = new KlapanSwitcher(mb, addr, this);
    klapan->setOnlySingleKlapanProtect(true);
    klapan->setRetryInterval(
                SettingsCmdLine::settings->value("Klapans/RefreshKlapanStateDelay_ms").toUInt());
    lKlapans.append(klapan);
}

QList<GST *> GSTSystem::getGST() const
{
    return lGst;
}

QList<DB00 *> GSTSystem::getSensors() const
{
    return lSensors;
}

QList<KlapanSwitcher *> GSTSystem::getKlapans() const
{
    return lKlapans;
}

bool GSTSystem::setActiveSensor(uint8_t addres)
{
    foreach (DB00* sensor, lSensors)
        if (sensor->getAddres() == addres)
        {
            ActiveSensor = sensor;
            return true;
        }
    LOG_ERROR(trUtf8("No sensors at addres %1").arg(addres, 2, 16, QChar('0')));
    return false;
}

bool GSTSystem::TestMinimalRequred()
{
    return ((lGst.size() >= 1) && (lSensors.size() >=1));
}

void GSTSystem::removeUnconnectedDevices()
{
    int i;
    for (i = 0; i < lGst.size(); ++i)
    {
        Sleeper::sleep_ms(50);
        QApplication::processEvents();
        if (!lGst.at(i)->Test())
        {
            LOG_ERROR(trUtf8("GST at adress %1 not ansvered, removing.").arg(lGst.at(i)->getAddres(), 2, 16, QChar('0')));
            delete lGst.at(i);
            lGst.removeAt(i);
        }
    }

    for (i = 0; i < lSensors.size(); ++i)
    {
        Sleeper::sleep_ms(50);
        QApplication::processEvents();
        if (!lSensors.at(i)->Test())
        {
            LOG_ERROR(trUtf8("Sensor at adress %1 not ansvered, removing.").arg(lSensors.at(i)->getAddres(), 2, 16, QChar('0')));
            delete lSensors.at(i);
            lSensors.removeAt(i);
        }
    }
    for (i = 0; i < lKlapans.size(); ++i)
    {
        Sleeper::sleep_ms(50);
        QApplication::processEvents();
        if (!lKlapans.at(i)->Test())
        {
            LOG_ERROR(trUtf8("Klapan kontroller at adress %1 not ansvered, removing.").arg(lKlapans.at(i)->getAddres(), 2, 16, QChar('0')));
            delete lKlapans.at(i);
            lKlapans.removeAt(i);
        }
    }
}

void GSTSystem::SaveConfiguration()
{
    (*SettingsCmdLine::settings)["LastSucess/Port"] = SettingsCmdLine::settings->value("Global/Port").toString();
    int i;
    for (i = 0; i < lGst.size(); ++i)
    {
        (*SettingsCmdLine::settings)[trUtf8("LastSucess/GST%1Addr").arg(i)] = lGst.at(i)->getAddres();
    }
    for (i = 0; i < lSensors.size(); ++i)
    {
        (*SettingsCmdLine::settings)[trUtf8("LastSucess/Sensor%1Addr").arg(i)] = lSensors.at(i)->getAddres();
    }
    if (lKlapans.size() > 0)
        (*SettingsCmdLine::settings)["LastSucess/KlapanCtrlAddr"] = lKlapans.at(0)->getAddres();
}

QBitArray GSTSystem::getInputs(bool *ok)
{
    if (ActiveKlapan == NULL)
    {
        if (lKlapans.size() > 0)
            ActiveKlapan = lKlapans.at(0);
        else
        {
            if (ok)
                *ok = false;
            return QBitArray();
        }
    }
    return ActiveKlapan->getInputs(ok);
}


bool GSTSystem::switchKlapan(uint16_t klapan, bool newstate)
{
    if (ActiveKlapan == NULL)
    {
        if (lKlapans.size() > 0)
            ActiveKlapan = lKlapans.at(0);
        else
            return false;
    }
    return ActiveKlapan->switchKlapan(klapan, newstate);
}

bool GSTSystem::closeAllKlapans()
{
    if (ActiveKlapan == NULL)
    {
        if (lKlapans.size() > 0)
            ActiveKlapan = lKlapans.at(0);
        else
            return false;
    }
    return ActiveKlapan->closeAll();
}

float GSTSystem::getPreasure(bool *ok)
{
    if (ActiveSensor == NULL)
    {
        if (lSensors.size() > 0)
            ActiveSensor = lSensors.at(0);
        else
        {
            if (ok)
                *ok = false;
            return 0.0;
        }
    }
    return ActiveSensor->getPreasure(ok);
}


unsigned char GSTSystem::getAllGSTData(GSTSystem::GSTData *pOutData) const
{
    unsigned char okFlags = 0;
    unsigned char counter = 0;
    foreach (GST* gst, lGst)
    {
        bool ok;
        QList<float> mesuredVals = gst->getOutputValues(&ok);
        if (ok && mesuredVals.size() == 2)
        {
            pOutData[counter].F = mesuredVals.at(0);
            pOutData[counter].Rk = mesuredVals.at(1);
            okFlags |= 1 << counter;
        }
        QApplication::processEvents();
        ++counter;
    }
    return okFlags;
}


bool GSTSystem::setActiveKlapanCtrl(uint8_t addres)
{
    foreach (KlapanSwitcher* klapan, lKlapans)
        if (klapan->getAddres() == addres)
        {
            ActiveKlapan = klapan;
            return true;
        }
    LOG_ERROR(trUtf8("No klapans at addres %1").arg(addres, 2, 16, QChar('0')));
    return false;
}
