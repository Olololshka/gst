#include <QApplication>
#include <QIcon>
#include <QStyle>
#include <QDesktopWidget>

#include <mylog/mylog.h>
#include <qmodbus/ModbusEngine.h>

#include "mainform.h"
#include "Settings.h"
#include "gstversiondeterminator.h"
#include "readexistingdatafromfile.h"
#include "gstgraph.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName("GST");
    app.setWindowIcon(QIcon(":/res/rkma.png"));

    Settings settings(argc, argv);
    if (!settings.isOk())
        return -1;

    MyLog::myLog log;
#ifndef NDEBUG
    log.setLogLevel(MyLog::LOG_DEBUG);
#endif

    ModbusEngine ModbusCore;

    if (settings.value("Global/Openonly").toString().isEmpty())
    {
        GSTVersionDeterminator detector(&ModbusCore);
        int res = detector.exec();

        switch (res)
        {
        case QDialog::Rejected:
            LOG_FATAL("Detecting devices failed, exiting...");
            return 0;
        case QDialog::Accepted:
        {
            mainform MainForm(detector.getSystem());
            MainForm.show();
            return app.exec();
        }
        default:
            return res;
        }
    }
    else
    {
        QString filename(settings.value("Global/Openonly").toString());
        LOG_INFO(QObject::trUtf8("Opening file: %1").arg(filename));
        GSTGraph* graph = ReadExistingDataFromFile::OpenExistingData(filename);
        if (graph != NULL)
        {
            graph->resize(WINDOW_DEF_H, WINDOW_DEF_W);
            graph->setGeometry(
                        QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, graph->size(),
                                            qApp->desktop()->availableGeometry()));
            graph->show();
            return app.exec();
        }
        else
        {
            return 10;
        }
    }
}
