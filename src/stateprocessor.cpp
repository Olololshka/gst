#include <math.h>

#include <QMessageBox>

#include <settingscmdline/settingscmdline.h>

#include <mylog/mylog.h>

#include "stateprocessor.h"

StateProcessor::StateProcessor(QObject *parent) :
    QObject(parent)
{
    CurentState = IDLE;
    setWorkRange(100, 800);
    preasureState = PreasureState::IDLE;
    curentPreasure = 0;
}

void StateProcessor::Start()
{
    lastMode = false;
    CurentState = DETECTING_STATE;
    emit SwitchDisplayModeReq(false);
    emit progress(trUtf8("Определение состояния.."), 0);
    tryCounter = CalculeTrysCount();
}

void StateProcessor::Stop()
{
    CurentState = IDLE;
    emit progress(trUtf8("Остановлено"), 0);
}

bool StateProcessor::setWorkRange(float Min, float Max)
{
    if (CurentState != IDLE)
        return false;
    else
    {
        startPreasure = Min;
        endpreasure = Max;
        return true;
    }
}

void StateProcessor::setAtmospherePreasure(float p)
{
    atmospherePreasure = p;
}

void StateProcessor::UpdateState(PreasureState::PreasureStates newState, float newPreasure)
{
    preasureState = newState;
    curentPreasure = newPreasure;
    const int _trys = CalculeTrysCount();

    switch (CurentState)
    {
    case IDLE:
        DisplayMeasureRk(false);
        emit progress(trUtf8("Ожидание"), 0);
        break;

    case REQUEST:
        DisplayMeasureRk(false);
        if (newState & requestedAction)
        {
            StageStartPreasure = curentPreasure;
            CurentState = IN_PROGRESS;
        }
        else
        {
            if (--tryCounter == 0)
            {
                //Что-то неоткрылось, давление не меняется
                if (QMessageBox::Retry == QMessageBox::question(NULL, trUtf8("Давление не изменяется"),
                                                                trUtf8("Обнаружено, что давление не изменяется, возможно система неисправна.\nХотите попробовать снова?"),
                                                                QMessageBox::Retry | QMessageBox::Cancel, QMessageBox::Retry))
                    // повтор
                {
                    requestPreasureChange(LastRequestStr, requestedAction, lastValveReq);
                    break;
                }
                else
                {
                    // Отмена
                    Stop();
                    emit Interrupted();
                    break;
                }
            }
            emit progress(trUtf8("Ожидание начала %1 давления...").arg(
                              requestedAction == PreasureState::UPING ?
                                  trUtf8("подъема") :
                                  trUtf8("снижения")), toPersent(0, _trys, _trys - tryCounter));
        }
        break;

    case IN_PROGRESS:
        if (requestedAction & PreasureState::UPING)
        {
            // подем может быть:
            // от стартового к атмосфере
            if ((lastValveReq == OPEN_ATM) && (startPreasure > atmospherePreasure))
            {
                if (newState & PreasureState::NEAR_ATM)
                {
                    emit progress(trUtf8("Атмосферное давлеине"), 100);
                    CurentState = ATM;
                }
                else
                    emit progress(trUtf8("Впуск атмосферного давления"), toPersent(StageStartPreasure, atmospherePreasure, curentPreasure));
                break;
            }
            // от начального к конечному
            // от стартового к начальному
            if (StageStartPreasure > startPreasure) // засчет гистерезиса тут все ломается
            {
                // прямой рабочий ход
                DisplayMeasureRk(true);
                if (curentPreasure <= endpreasure)
                {
                    emit progress(trUtf8("Натекание"), toPersent(startPreasure, endpreasure, curentPreasure));
                    break;
                }
                else
                {
                    CurentState = READY;
                    emit progress(trUtf8("Конечное давление достигнуто"), 100);
                    emit ValveRequest(trUtf8("Закройте клапан подачи давления"), CLOSE_ALL);
                    break;
                }
            }
            else
            {
                // нагнетание до старта
                DisplayMeasureRk(false);
                if (curentPreasure >= startPreasure)
                {
                    emit progress(trUtf8("Начальное давление достигнуто"), 100);
                    if (startPreasure < endpreasure)
                    {   // продолжить
                        StageStartPreasure = curentPreasure;
                    }
                    else
                    {
                        if ((startPreasure > endpreasure) && (endpreasure < atmospherePreasure))
                        {
                            requestPreasureChange(trUtf8("Откройте клапан подачи вакуума"), PreasureState::FALLING, OPEN_VACUUM);
                        }
                        else
                        {
                            if (endpreasure < atmospherePreasure)
                                requestPreasureChange(trUtf8("Откройте клапан подачи вакуума"), PreasureState::FALLING, OPEN_VACUUM);
                            else
                                requestPreasureChange(trUtf8("Откройте атмосферный клапан"), PreasureState::FALLING, OPEN_ATM);
                        }
                    }
                    break;
                }
                else
                {
                    emit progress(trUtf8("Натекание"), toPersent(StageStartPreasure, startPreasure, curentPreasure));
                    break;
                }
            }
        }
        if (requestedAction & PreasureState::FALLING)
        {
            //  снижеине
            //  сброс давления, передс откачкой в атмосферу
            if (lastValveReq == OPEN_ATM)
            {
                if (startPreasure > atmospherePreasure)
                {
                    // либо рабочий ход назад
                    DisplayMeasureRk(curentPreasure < startPreasure); // измерять, если прошли начало
                    if (curentPreasure < endpreasure)
                    {
                        CurentState = READY;
                        emit progress(trUtf8("Конечное давление достигнуто"), 100);
                    }
                    else
                        emit progress(trUtf8("Стекание давления в атмосферу"), toPersent(atmospherePreasure, StageStartPreasure, curentPreasure));
                }
                else
                {
                    //либо для подготовки к вакуумированию
                    DisplayMeasureRk(false);
                    if (newState & PreasureState::NEAR_ATM)
                    {
                        emit progress(trUtf8("Давлеине сброшено"), 100);
                        CurentState = ATM;
                    }
                    else
                        emit progress(trUtf8("Сброс давления в атмосферу"), toPersent(atmospherePreasure, StageStartPreasure, curentPreasure));
                }
                break;
            }
            if (startPreasure < endpreasure)
            {
                //  перед стартом
                DisplayMeasureRk(false);
                if (curentPreasure > startPreasure)
                    emit progress(trUtf8("Откачка давления"), toPersent(startPreasure, StageStartPreasure, curentPreasure));
                else
                {
                    emit progress(trUtf8("Откачка завершена"), 100);
                    requestPreasureChange(trUtf8("Откройте клапан подачи давления"), PreasureState::UPING, OPEN_PREASURE);
                }
                break;
            }
            else
            {
                //  рабочий ход на снижение
                DisplayMeasureRk(true);
                if (curentPreasure > endpreasure)
                    emit progress(trUtf8("Стекание давления"), toPersent(endpreasure, startPreasure, curentPreasure));
                else
                {
                    CurentState = READY;
                    emit progress(trUtf8("Конечное давление достигнуто"), 100);
                    emit ValveRequest(trUtf8("Закройте %1 клапан").arg(
                                          curentPreasure < atmospherePreasure ? trUtf8("вакуумный") : trUtf8("атмосферный")),
                                      CLOSE_ALL);
                    break;
                }
            }
        }
        break;

    case DETECTING_STATE:
        DisplayMeasureRk(false);
        if (!(newState & PreasureState::STABLE))
        {
            // нестабильно, ждем
            --tryCounter;
            if (tryCounter == 0)
            {
                emit progress(trUtf8("Давление нестабильно"), 100);
                emit Interrupted();
                CurentState = IDLE;
                break;
            }
            emit progress(trUtf8("Определение состояния.."), toPersent(0, _trys, _trys - tryCounter));
        }
        else
        {
            tryCounter = _trys;
            if (newState & PreasureState::NEAR_ATM)
                // атмосфера
                CurentState = ATM;
            else
            {
                if (newState & PreasureState::MORE_ATM)
                    // больше атмосферы
                    CurentState = MORE_THEN_ATM;
                else
                {
                    if (newState & PreasureState::LESS_ATM)
                        // меньше атмосферы
                        CurentState = LESS_THAN_ATM;
                    else
                    {
                        emit Interrupted();
                        emit progress(trUtf8("Ошибка определения начального состояния"), 100);
                        CurentState = IDLE;
                        break;
                    }
                }
            }
        }
        break;
    case MORE_THEN_ATM:
        // - куда двигаться?
        // - в сторону начального давления!
        StageStartPreasure = newPreasure;
        if (newPreasure <= startPreasure)
            // нужно накачивать,
            requestPreasureChange(trUtf8("Откройте клапан подачи давления"), PreasureState::UPING, OPEN_PREASURE);
        else
        {
            // нужно спускать
            if (startPreasure < atmospherePreasure)
                // спустить все в атмосферу
                requestPreasureChange(trUtf8("Выровняйте давление в камере с атмосферным"), PreasureState::FALLING, OPEN_ATM);
            else
                requestPreasureChange(trUtf8("Откройте клапан стекания в атмосферу"), PreasureState::FALLING, OPEN_ATM);
        }
        break;
    case ATM:
        StageStartPreasure = newPreasure;
        if (newPreasure <= startPreasure)
            // нужно накачивать,
            requestPreasureChange(trUtf8("Откройте клапан подачи давления"), PreasureState::UPING, OPEN_PREASURE);
        else
            // нужно откачивать
            requestPreasureChange(trUtf8("Откройте клапан подачи вакуума"), PreasureState::FALLING, OPEN_VACUUM);
        break;
    case LESS_THAN_ATM:
        StageStartPreasure = newPreasure;
        if (newPreasure <= startPreasure)
        {
            if (startPreasure > atmospherePreasure)
                // сразу открываем атмосферу
                requestPreasureChange(trUtf8("Откройте атмосферный клапан"), PreasureState::UPING, OPEN_ATM);
            else
            {
                if ((endpreasure < atmospherePreasure) && (startPreasure < endpreasure))
                    // накачка из атмосферы через натекатель
                    requestPreasureChange(trUtf8("Откройте клапан атмосферного натекателя"), PreasureState::UPING, OPEN_ATM);
                else
                    // нужно накачивать через натекатель
                    requestPreasureChange(trUtf8("Откройте клапан подачи давления"), PreasureState::UPING, OPEN_PREASURE);
            }
        }
        else
            // нужно откачивать дальше
            requestPreasureChange(trUtf8("Откройте клапан подачи вакуума"), PreasureState::FALLING, OPEN_VACUUM);
        break;
    case READY:
        DisplayMeasureRk(true);
        emit MeasureFinished();
        CurentState = IDLE;
        break;
    }
}


int StateProcessor::toPersent(float min, float max, float value)
{
    int res = lround((value - min) / (max - min) * 100);
    return res < 0 ? 0 : res;
}

void StateProcessor::DisplayMeasureRk(bool mode)
{
    if (mode != lastMode)
    {
        lastMode = mode;
        emit SwitchDisplayModeReq(mode);
    }
}

int StateProcessor::CalculeTrysCount()
{
    int res = lround(SettingsCmdLine::settings->value("Interface/ActionWaitTime").toInt() /
                     SettingsCmdLine::settings->value("Measure/UpdateInterval_ms").toDouble() * 1000);
    return res;
}

void StateProcessor::requestPreasureChange(const QString& requestInfo, PreasureState::PreasureStates action, valveReq ValveReq)
{
    lastValveReq = ValveReq;
    LastRequestStr = requestInfo;
    CurentState = REQUEST;
    requestedAction = action;
    tryCounter = CalculeTrysCount();
    emit ValveRequest(requestInfo, ValveReq);
}

