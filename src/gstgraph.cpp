#include <stdint.h>
#include <vector>
#include <iterator>
#include <limits>

#include <QCheckBox>
#include <QSpinBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QAction>
#include <QFileDialog>
#include <QDir>
#include <QFile>
#include <QDateTime>
#include <QTextCodec>
#include <QtAlgorithms>
#include <QMutableListIterator>
#include <QApplication>
#include <QPrintDialog>
#include <QPrinter>

#include <qwt_plot_curve.h>
#include <qwt_legend.h>
#include <qwt_scale_widget.h>
#include <qwt_legend_data.h>
#include <qwt_symbol.h>
#include <qwt_legend_label.h>
#include <qwt_plot_renderer.h>
#include <qwt_plot_textlabel.h>

#include <qmodbus/common.h>
#include <settingscmdline/settingscmdline.h>
#include <mylog/mylog.h>

#include "stocktimescaledraw.h"
#include "interpolation.h"
#include "ap.h"

#include "inputlimits.h"
#include "slidemiddlefilter.h"

#include "gstgraph.h"

GSTGraph::GSTGraph(QWidget *parent) :
    QWidget(parent)
{
    CurentMode = Rk_P_F_OF_T;
    fShowLasthistory = false;
    unsaved = false;

    plot = new QwtPlot;
    plot->setCanvasBackground(QColor(Qt::white));
    plot->setStyleSheet("background-color:white;");

    label_sensitiveCoeff = new QwtPlotTextLabel;

    QPen pen;
    QwtSymbol* symbol;

    symbol = new QwtSymbol;
    pen = QPen(Qt::black);
    symbol->setStyle(QwtSymbol::Triangle);
    symbol->setPen(pen);
    symbol->setSize(5);
    preasure_T_Curve = new QwtPlotCurve(trUtf8("P(t)"));
    preasure_T_Curve->setRenderHint(QwtPlotItem::RenderAntialiased);
    preasure_T_Curve->setPen(pen);
    preasure_T_Curve->setSymbol(symbol);

    symbol = new QwtSymbol;
    pen = QPen(Qt::darkGreen);
    symbol->setStyle(QwtSymbol::XCross);
    symbol->setPen(pen);
    symbol->setSize(5);
    Rk_T_Curve = new QwtPlotCurve(trUtf8("Rk(t)"));
    Rk_T_Curve->setRenderHint(QwtPlotItem::RenderAntialiased);
    Rk_T_Curve->setPen(pen);
    Rk_T_Curve->setSymbol(symbol);

    symbol = new QwtSymbol;
    pen = QPen(Qt::blue);
    symbol->setStyle(QwtSymbol::Cross);
    symbol->setPen(pen);
    symbol->setSize(5);
    FTCurve = new QwtPlotCurve(trUtf8("F(t)"));
    FTCurve->setRenderHint(QwtPlotItem::RenderAntialiased);
    FTCurve->setPen(pen);
    FTCurve->setSymbol(symbol);

    symbol = new QwtSymbol;
    pen = QPen(Qt::darkRed);
    symbol->setStyle(QwtSymbol::XCross);
    symbol->setPen(pen);
    symbol->setSize(5);
    Rk_P_Curve = new QwtPlotCurve(trUtf8("Rk(P)"));
    Rk_P_Curve->setRenderHint(QwtPlotItem::RenderAntialiased);
    Rk_P_Curve->setPen(pen);
    Rk_P_Curve->setSymbol(symbol);

    symbol = new QwtSymbol;
    pen = QPen(Qt::darkGray);
    symbol->setStyle(QwtSymbol::UTriangle);
    symbol->setPen(pen);
    symbol->setSize(5);
    F_P_Curve = new QwtPlotCurve(trUtf8("F(P)"));
    F_P_Curve->setRenderHint(QwtPlotItem::RenderAntialiased);
    F_P_Curve->setPen(pen);
    F_P_Curve->setSymbol(symbol);

    // погрешность
    symbol = new QwtSymbol;
    pen = QPen(Qt::darkCyan);
    pen.setWidth(2);
    symbol->setStyle(QwtSymbol::UTriangle);
    symbol->setPen(pen);
    symbol->setSize(5);
    FreqErrorCurve = new QwtPlotCurve(trUtf8("F - Fapr"));
    FreqErrorCurve->setRenderHint(QwtPlotItem::RenderAntialiased);
    FreqErrorCurve->setPen(pen);
    FreqErrorCurve->setSymbol(symbol);

    // трубка точности
    symbol = new QwtSymbol;
    pen = QPen(Qt::green);
    pen.setWidth(2);
    symbol->setStyle(QwtSymbol::NoSymbol);
    symbol->setPen(pen);
    symbol->setSize(5);
    Middleline = new QwtPlotCurve(trUtf8("~Rk"));
    Middleline->setRenderHint(QwtPlotItem::RenderAntialiased);
    Middleline->setPen(pen);
    Middleline->setSymbol(symbol);

    // если присоединить один и тот же символ к обоим курвам, то сегфолт при удалении второго, тк символ уже удален
    pen = QPen(Qt::red);
    symbol = new QwtSymbol;
    symbol->setStyle(QwtSymbol::NoSymbol);
    symbol->setPen(pen);
    symbol->setSize(5);
    AcuracyPipeUpLine = new QwtPlotCurve(trUtf8("~RK +"));
    AcuracyPipeUpLine->setRenderHint(QwtPlotItem::RenderAntialiased);
    AcuracyPipeUpLine->setPen(pen);
    AcuracyPipeUpLine->setSymbol(symbol);
    //symbol = new QwtSymbol(*symbol); //копия (копировать влоб запрещено)
    symbol = new QwtSymbol;
    symbol->setStyle(QwtSymbol::NoSymbol);
    symbol->setPen(pen);
    symbol->setSize(5);
    AcuracyPipeDownLine = new QwtPlotCurve(trUtf8("~RK -"));
    AcuracyPipeDownLine->setRenderHint(QwtPlotItem::RenderAntialiased);
    AcuracyPipeDownLine->setPen(pen);
    AcuracyPipeDownLine->setSymbol(symbol);
    //---

    legend = new QwtLegend;
    legend->setDefaultItemMode(QwtLegendData::Checkable);
    plot->insertLegend(legend);

    connect(legend, SIGNAL(checked(QVariant,bool,int)), this,
            SLOT(showCurve(QVariant,bool,int)));

    plot->enableAxis(QwtPlot::yRight);
    plot->setAxisTitle(QwtPlot::yRight, trUtf8("Частота [Гц]"));

    QVBoxLayout *rootlayout = new QVBoxLayout;
    rootlayout->addWidget(plot);

    onlylastMeasures = new QCheckBox(trUtf8("Последние: "));
    connect(onlylastMeasures, SIGNAL(clicked()), this, SLOT(replot()));

    filterFError = new QCheckBox(trUtf8("Фильтровать график F - Fapr"));
    filterFError->setVisible(false);
    connect(filterFError, SIGNAL(clicked()), this, SLOT(replot()));

    numofLastOnes = new QSpinBox;
    numofLastOnes->setMinimum(MINIMAL_HISTORY_LEN);
    numofLastOnes->setMaximum(1000);
    numofLastOnes->setValue(SettingsCmdLine::settings->value("Graph/HistoryLen", 100).toInt());
    connect(numofLastOnes, SIGNAL(valueChanged(int)), this, SLOT(saveDefaultHistoryLen(int)));

    labelPreasureChengeSpeed = new QLabel(trUtf8("Изменение давления:"));

    QHBoxLayout* l = new QHBoxLayout;
    l->addWidget(labelPreasureChengeSpeed);
    l->addStretch();
    l->addWidget(filterFError);
    l->addWidget(onlylastMeasures);
    l->addWidget(numofLastOnes);

    rootlayout->addLayout(l);

    setLayout(rootlayout);

    AClear = new QAction(QIcon(":/res/clear.PNG"), trUtf8("Очистить"), this);
    ASaveData = new QAction(QIcon(":/res/saveData.png"), trUtf8("Сохранить данные в файл"), this);
    ASavePicture = new QAction(QIcon(":/res/savePen.png"), trUtf8("Сохранить как картинку"), this);
    APrintGraph = new QAction(QIcon(":/res/printer.png"), trUtf8("Распечатать график"), this);
    ADisplayFragment = new QAction(trUtf8("Показать только фрагмент"), this);
    ADisplayFragment->setCheckable(true);
    ADisplayFragment->setVisible(false);

    connect(AClear, SIGNAL(triggered()), this, SLOT(clean()));
    connect(ASaveData, SIGNAL(triggered()), this, SLOT(saveGraphData()));
    connect(ASavePicture, SIGNAL(triggered()), this, SLOT(saveGraphImage()));
    connect(APrintGraph, SIGNAL(triggered()), this, SLOT(printGraph()));
    connect(ADisplayFragment, SIGNAL(triggered(bool)), this, SLOT(ShowFragment(bool)));

    plot->addAction(AClear);
    plot->addAction(ASaveData);
    plot->addAction(ASavePicture);
    plot->addAction(APrintGraph);
    plot->addAction(ADisplayFragment);
    plot->setContextMenuPolicy(Qt::ActionsContextMenu);

    setMode(Rk_P_F_OF_T);
    resize(640, 480);
}

GSTGraph::~GSTGraph()
{
    __DELETE(legend);

    // курвы удалить все же нужно, плот удалит только присоединенные в данный момент
    __DELETE(labelPreasureChengeSpeed);

    __DELETE(FreqErrorCurve);

    __DELETE(Middleline);
    __DELETE(AcuracyPipeUpLine);
    __DELETE(AcuracyPipeDownLine);

    __DELETE(preasure_T_Curve);
    __DELETE(Rk_T_Curve);
    __DELETE(FTCurve);

    __DELETE(Rk_P_Curve);
    __DELETE(F_P_Curve);
}

void GSTGraph::clean()
{
    Vpreasure_T_Curve.clear();
    VRk_T_Curve.clear();
    VF_T_Curve.clear();

    VRk_P_Curve.clear();
    VF_P_Curve.clear();

    vError_points.clear();
    label_sensitiveCoeff->setText(QString());

    unsaved = false;
}

void GSTGraph::addData(float P, float Rk, float F, QTime timestamp)
{
    int msec = QTime().msecsTo(timestamp);

    if (!Vpreasure_T_Curve.isEmpty())
        labelPreasureChengeSpeed->setText(
                    trUtf8("Изменение давления: %1 [%2/с]").arg(
                        (P - Vpreasure_T_Curve.last().y()) /
                        SettingsCmdLine::settings->value("Measure/UpdateInterval_ms").toDouble() * 1000, 0, 'f', 4).arg(
                        SettingsCmdLine::settings->value("Measure/PreasureUnitsName").toString()));
    else
        labelPreasureChengeSpeed->setText(trUtf8("Изменение давления: -"));

    Vpreasure_T_Curve.append(QPointF(msec, P));
    VRk_T_Curve.append(QPointF(msec, Rk));
    VF_T_Curve.append(QPointF(msec, F));

    VRk_P_Curve.append(QPointF(P, Rk));
    VF_P_Curve.append(QPointF(P, F));

    replot();
}

void GSTGraph::setMode(GSTGraph::GraphMode mode)
{
    QString pU = SettingsCmdLine::settings->value("Measure/PreasureUnitsName").toString();
    switch (mode)
    {
    case Rk_P_F_OF_T:
        plot->setAxisTitle(QwtPlot::xBottom, trUtf8("Время"));
        plot->setAxisTitle(QwtPlot::yLeft, trUtf8("Давление [%1] / Rk [кОм]").arg(pU));

        plot->setAxisScaleDraw(QwtPlot::xBottom, new StockTimeScaleDraw(trUtf8("mm.ss")));

        filterFError->setVisible(false);

        Rk_P_Curve->detach();
        F_P_Curve->detach();

        Middleline->detach();
        AcuracyPipeUpLine->detach();
        AcuracyPipeDownLine->detach();

        FreqErrorCurve->detach();

        label_sensitiveCoeff->detach();

        ADisplayFragment->setVisible(false);

        preasure_T_Curve->attach(plot);
        Rk_T_Curve->attach(plot);
        FTCurve->attach(plot);
        FTCurve->setAxes(QwtPlot::xBottom, QwtPlot::yRight);

        showCurve(plot->itemToInfo(preasure_T_Curve), true, 0);
        showCurve(plot->itemToInfo(Rk_T_Curve), true, 0);
        showCurve(plot->itemToInfo(FTCurve), true, 0);

        CurentMode = mode;
        break;
    case RK_F_OF_P:
        // clean this curves
        VRk_P_Curve.clear();
        VF_P_Curve.clear();

        VMiddleline.clear();
        vError_points.clear();

        label_sensitiveCoeff->setText(QString());

        filterFError->setVisible(true);

        plot->setAxisTitle(QwtPlot::xBottom, trUtf8("Давление [%1]").arg(pU));
        plot->setAxisScaleDraw(QwtPlot::xBottom, new QwtScaleDraw);
        plot->setAxisTitle(QwtPlot::yLeft, trUtf8("Rk [кОм]"));

        preasure_T_Curve->detach();
        Rk_T_Curve->detach();
        FTCurve->detach();

        Rk_P_Curve->attach(plot);
        F_P_Curve->attach(plot);
        F_P_Curve->setAxes(QwtPlot::xBottom, QwtPlot::yRight);

        Middleline->attach(plot);
        AcuracyPipeUpLine->attach(plot);
        AcuracyPipeUpLine->setTitle(trUtf8("~RK + %1%").arg(
                                        SettingsCmdLine::settings->value("Measure/AcuracyPipe_pr").toDouble()));
        AcuracyPipeDownLine->attach(plot);
        AcuracyPipeDownLine->setTitle(trUtf8("~RK - %1%").arg(
                                          SettingsCmdLine::settings->value("Measure/AcuracyPipe_pr").toDouble()));

        FreqErrorCurve->attach(plot);
        FreqErrorCurve->setAxes(QwtPlot::xBottom, QwtPlot::yRight);

        label_sensitiveCoeff->attach(plot);

        ADisplayFragment->setVisible(true);

        showCurve(plot->itemToInfo(Rk_P_Curve), true, 0);
        showCurve(plot->itemToInfo(F_P_Curve), true, 0);

        showCurve(plot->itemToInfo(Middleline), false, 0);
        showCurve(plot->itemToInfo(AcuracyPipeUpLine), false, 0);
        showCurve(plot->itemToInfo(AcuracyPipeDownLine), false, 0);

        showCurve(plot->itemToInfo(Middleline), false, 0);

        showCurve(plot->itemToInfo(FreqErrorCurve), false, 0);

        if (CurentMode == Rk_P_F_OF_T) // если начали измерение Rк (свич режим), то значит есть что схоронить
            unsaved = true;

        CurentMode = mode;
        break;
    default:
        return;
    }
    replot();
}

void GSTGraph::showCurve(QVariant iteminfo, bool on, int index)
{
    Q_UNUSED(index);
    showCurveNoReploat(iteminfo, on);
    plot->replot();
}

void GSTGraph::showCurveNoReploat(QVariant iteminfo, bool on)
{
    QwtPlotItem *item = plot->infoToItem(iteminfo);
    item->setVisible(on);
    if (legend->legendWidget(iteminfo)->inherits("QwtLegendLabel"))
        ((QwtLegendLabel*)legend->legendWidget(iteminfo))->setChecked(on);
}

void GSTGraph::ShowFragment(bool show)
{
    if (show)
    {
        //sort
        QVector<QPointF> F_P_Data = SortPoints(VRk_P_Curve);

        inputLimits limits(F_P_Data.first().x(), F_P_Data.last().x(), this);
        limits.setModal(true);
        if (limits.exec() == QDialog::Accepted)
        {
            plot->setAxisScale(QwtPlot::xBottom, limits.from(), limits.to());
            plot->replot();
        }
    }
    else
    {
        // показать все
        replot();
    }
}

void GSTGraph::SetHistoryLen(int len)
{
    numofLastOnes->setValue(len);
}

void GSTGraph::replot()
{
    if (onlylastMeasures->isChecked())
    {
        // только последние
        switch(CurentMode)
        {
        case Rk_P_F_OF_T:
        {
            int pointsNum = numofLastOnes->value();
            std::vector<QPointF> v;
            if (pointsNum < Vpreasure_T_Curve.size())
            {
                int startPos = Vpreasure_T_Curve.size() - pointsNum;

                v.assign(&Vpreasure_T_Curve.data()[startPos], &Vpreasure_T_Curve.last());
                preasure_T_Curve->setSamples(
                            QVector<QPointF>::fromStdVector(v));
                v.assign(&VRk_T_Curve.data()[startPos], &VRk_T_Curve.last());
                Rk_T_Curve->setSamples(
                            QVector<QPointF>::fromStdVector(v));
                v.assign(&VF_T_Curve.data()[startPos], &VF_T_Curve.last());
                FTCurve->setSamples(
                            QVector<QPointF>::fromStdVector(v));

                if (!v.empty())
                    plot->setAxisScale(QwtPlot::xBottom, v.at(0).x(), (v.end() - 1)->x());
            }
            else
                plotAll();
        }
            break;
        case RK_F_OF_P:
        {
            int pointsNum = numofLastOnes->value();
            std::vector<QPointF> v;
            if (pointsNum > VRk_P_Curve.size())
            {
                int startPos = VRk_P_Curve.size() - pointsNum;

                v.assign(&VRk_P_Curve.data()[startPos], &VRk_P_Curve.last());
                Rk_P_Curve->setSamples(QVector<QPointF>::fromStdVector(v));

                v.assign(&VF_P_Curve.data()[startPos], &VF_P_Curve.last());
                F_P_Curve->setSamples(QVector<QPointF>::fromStdVector(v));

                if (!v.empty())
                    plot->setAxisScale(QwtPlot::xBottom, v.at(0).x(), (v.end() - 1)->x());
            }
            else
                plotAll();
        }
            break;
        }
    }
    else
        //все
        plotAll();
    plot->replot();
}

void GSTGraph::showCustomRk_P_graph(const QVector<QPointF> &Rk_Data, const QVector<QPointF>& F_Data)
{
    if (Rk_Data.size() != F_Data.size())
    {
        LOG_ERROR("Size of custom graph data not equal, reject");
        return;
    }

    setMode(RK_F_OF_P);
    VRk_P_Curve = Rk_Data;
    VF_P_Curve = F_Data;

    numofLastOnes->hide();
    onlylastMeasures->hide();
    labelPreasureChengeSpeed->hide();
    replot();
}

void GSTGraph::BuildRkAccuracyPipe()
{
    float Rk_min = 100000;
    float Rk_Max = -100000;

    int hystogramFieldsNum = SettingsCmdLine::settings->value("Measure/HystogramFields").toInt();
    float minimalInterval = SettingsCmdLine::settings->value("Measure/MinimalPreasureP2PInterval").toFloat();

    QVector<QPointF> Rk_P_Data = SortPoints(VRk_P_Curve);
    QMutableVectorIterator<QPointF> it(Rk_P_Data);

    //Rk_Data теперь отсортирован по P
    float P_min = Rk_P_Data.first().x();
    float P_max = Rk_P_Data.last().x();

    // отбрасывание слишком близкие точки
    bool f = true;
    while(f)
    {
        f = false;
        it.toFront();
        while (true)
        {
            float a = it.next().x();
            if (!it.hasNext())
                break;
            float b = it.peekNext().x();
            if (qAbs(b - a) < minimalInterval)
            {
                //it.next();
                it.remove();
                f = true;
                continue;
            }
        }
    }

    QList<float> Rk_Data;

    foreach(QPointF point, Rk_P_Data)
    {
        float Rk = point.y();
        Rk_Data.append(Rk);

        if (Rk < Rk_min)
            Rk_min = Rk;
        if (Rk > Rk_Max)
            Rk_Max = Rk;
    }

    LOG_DEBUG(trUtf8("Rk MIN/MAX: %1/%2\t P MIN/MAX: %3/%4").arg(Rk_min).arg(Rk_Max).arg(P_min).arg(P_max));

    float HystgramFieldStep = (Rk_Max - Rk_min) / (hystogramFieldsNum - 1);
    if (isnan(HystgramFieldStep) || HystgramFieldStep == 0)
        return; // ловушка касяков
    QVector<int> hystogramm(hystogramFieldsNum, 0);
    foreach(float Rk, Rk_Data) // создание гистограммы
        ++hystogramm[static_cast<int>(floor((Rk - Rk_min) / HystgramFieldStep))];

    QString hystogrammStr = trUtf8("Hystogramm: ");
    int HystoMaximum = 0;
    int HystoMaximumIndex = 0;
    for (int i = 0; i < hystogramFieldsNum; ++i)
    {
        if (hystogramm[i] > HystoMaximum)
        {
            HystoMaximum = hystogramm[i];
            HystoMaximumIndex = i;
        }
        hystogrammStr.append(QString::number(hystogramm[i]));
        if (i != hystogramFieldsNum - 1)
            hystogrammStr.append(", ");
    }
    float MajorIntervalMin = Rk_min + HystgramFieldStep * HystoMaximumIndex;
    float MajorIntervalMax = Rk_min + HystgramFieldStep * (HystoMaximumIndex + 1);
    LOG_DEBUG(hystogrammStr + trUtf8(". Major interval: %1 - %2").arg(MajorIntervalMin).arg(MajorIntervalMax));

    float midleLevel = 0;
    foreach(float Rk, Rk_Data)
        if ((Rk >= MajorIntervalMin) && (Rk < MajorIntervalMax))
            midleLevel += Rk;

    midleLevel /= HystoMaximum;

    LOG_DEBUG(trUtf8("Middle level = %1").arg(midleLevel));

    VMiddleline.clear();

    VMiddleline.append(QPointF(P_min, midleLevel));
    VMiddleline.append(QPointF(P_max, midleLevel));

    replot();
}

void GSTGraph::BuildFErrorCurve()
{
    float minimalInterval = SettingsCmdLine::settings->value("Measure/MinimalPreasureP2PInterval").toFloat();
    QVector<QPointF> F_P_Data = SortPoints(VF_P_Curve);
    QMutableVectorIterator<QPointF> it(F_P_Data);

    // отбрасывание слишком близких точек

    bool f = true;
    while(f)
    {
        f = false;
        it.toFront();
        while (true)
        {
            float a = it.next().x();
            if (!it.hasNext())
                break;
            float b = it.peekNext().x();
            if (qAbs(b - a) < minimalInterval)
            {
                //it.next();
                it.remove();
                f = true;
                continue;
            }
        }
    }
    //разбиваем на 2 вектора, чтобы скормить потом alglib'у
    QVector<double> vP;
    QVector<double> vF;
    it.toFront();
    while (it.hasNext())
    {
        it.next();
        vP.append(it.value().x());
        vF.append(it.value().y());
    }

    if (vP.isEmpty() || vF.isEmpty())
        return;

    int dropLastPointsNum = SettingsCmdLine::settings->value("Measure/DropLastPoints").toInt();
    if (dropLastPointsNum + 2 > vP.size())
        return;
    vP.remove(vP.size() - dropLastPointsNum, dropLastPointsNum);
    vF.remove(vF.size() - dropLastPointsNum, dropLastPointsNum);

    double maxFreqChangeSpeed = SettingsCmdLine::settings->value("Measure/maxFreqChangeSpeed").toDouble();

    QVector<double> FreqChangeSpeed(vP.size());
    FreqChangeSpeed[0] = 0.0;
    for (int i = 1; i < vP.size(); ++i)
    {
        FreqChangeSpeed[i] =(vF.at(i - 1) - vF.at(i)) / (vP.at(i - 1) - vP.at(i));
    }
    QPointF removeRange;
    while((removeRange = FindMinMaxIndexes(FreqChangeSpeed, maxFreqChangeSpeed)) != QPoint(0, 0))
    {
        int from = removeRange.x();
        int count = removeRange.y() - from + 1;
        if (removeRange.x() > removeRange.y())
        {
            from = removeRange.y();
            count = 1;
        }
        FreqChangeSpeed.remove(from, count);
        vP.remove(from, count);
        vF.remove(from, count);
    }
    /*
    vError_points = QVector<QPointF>(vP.size());
    for(int i = 0; i < vP.size(); ++i)
        vError_points[i] = QPointF(vP.at(i), vF.at(i));
        */

    // апроксимация (библиотека ALGLIB пакет interpolation)
    alglib::real_1d_array al_vP, al_vF;
    al_vP.setcontent(vP.size(), vP.data()); // аргумент
    al_vF.setcontent(vF.size(), vF.data()); // значение

    // http://alglib.sources.ru/translator/man/manual.cpp.html#sub_polynomialfit
    alglib::ae_int_t m =
            SettingsCmdLine::settings->value("Measure/AproximatePolyPow").toInt() + 1; // степень получаемого полинома + 1
    alglib::ae_int_t info; // код ошибки ( > 0 все ок)
    alglib::barycentricinterpolant p; // здесь будет барицентрическое внутренне представление
    alglib::polynomialfitreport rep; // описание ошибки

    alglib::polynomialfit(al_vP, al_vF, m, info, p, rep); // выполняется апроксимация

    LOG_DEBUG("Aproximate step 1 arror report:");
    LOG_DEBUG(trUtf8("-> taskrcond: %1").arg(rep.taskrcond));
    LOG_DEBUG(trUtf8("-> rmserror: %1").arg(rep.rmserror));
    LOG_DEBUG(trUtf8("-> avgerror: %1").arg(rep.avgerror));
    LOG_DEBUG(trUtf8("-> avgrelerror: %1").arg(rep.avgrelerror));
    LOG_DEBUG(trUtf8("-> maxerror: %1").arg(rep.maxerror));

    // заполняем вектор ошибки апроксимации
    vError_points.clear();
    it.toFront();
    while(it.hasNext())
    {
        QPointF t = it.next();
        vError_points.append(QPointF(t.x(), t.y() - alglib::barycentriccalc(p, t.x())));
    }
    /*
    QVector<double>::const_iterator it_vP = vP.begin();
    QVector<double>::const_iterator it_vF = vF.begin();
    for (; it_vP < vP.end(); ++it_vP, ++it_vF)
        vError_points.append(QPointF(*it_vP, *it_vF - alglib::barycentriccalc(p, *it_vP)));
        //vError_points.append(QPointF(*it_vP, alglib::barycentriccalc(p, *it_vP)));
    */
    // коэфициенты
    alglib::real_1d_array coeffs;
    alglib::polynomialbar2pow(p, coeffs);
    LOG_INFO(trUtf8("Coeffs: %1").arg(QString::fromStdString(coeffs.tostring(5))));

    QwtText sensLabelText(trUtf8("Чувствительность: %1 [Гц/%2]").arg(
                              coeffs[1], 0, 'f', 3).arg(
                SettingsCmdLine::settings->value("Measure/PreasureUnitsName").toString()));
    sensLabelText.setRenderFlags(Qt::AlignTop | Qt::AlignLeft);
    QFont fontLebelSens;
    fontLebelSens.setBold(true);
    sensLabelText.setFont(fontLebelSens);

    label_sensitiveCoeff->setText(sensLabelText);
    //*/
    replot();
}

void GSTGraph::setGraphTitle(const QString &title)
{
    plot->setTitle(title);
}

void GSTGraph::AcceptNewSettings()
{
    if (CurentMode == RK_F_OF_P)
    {
        if (!VMiddleline.isEmpty())
            BuildRkAccuracyPipe();
        if (!vError_points.empty())
            BuildFErrorCurve();
    }
}

void GSTGraph::plotAll()
{
    switch(CurentMode)
    {
    case Rk_P_F_OF_T:
        preasure_T_Curve->setSamples(Vpreasure_T_Curve);
        Rk_T_Curve->setSamples(VRk_T_Curve);
        FTCurve->setSamples(VF_T_Curve);
        if (!Vpreasure_T_Curve.isEmpty())
            plot->setAxisScale(QwtPlot::xBottom, Vpreasure_T_Curve.at(0).x(), Vpreasure_T_Curve.last().x());
        break;
    case RK_F_OF_P:
        Rk_P_Curve->setSamples(VRk_P_Curve);
        F_P_Curve->setSamples(VF_P_Curve);
        if (!VRk_P_Curve.isEmpty())
            plot->setAxisScale(QwtPlot::xBottom, VRk_P_Curve.at(0).x(), VRk_P_Curve.last().x());
        if (!VMiddleline.isEmpty())
        { // нарисуем трубку
            QVector<QPointF> UplineSamples, DownLineSamples;
            float Acuracy_pr = SettingsCmdLine::settings->value("Measure/AcuracyPipe_pr", 1).toFloat();
            foreach (QPointF point, VMiddleline)
            {
                float P = point.x();
                float Rk = point.y();
                float tubeRadius = Rk * Acuracy_pr / 100.0;
                UplineSamples.append(QPointF(P, Rk + tubeRadius));
                DownLineSamples.append(QPointF(P, Rk - tubeRadius));
            }

            Middleline->setSamples(VMiddleline);
            AcuracyPipeUpLine->setSamples(UplineSamples);
            AcuracyPipeDownLine->setSamples(DownLineSamples);

            showCurveNoReploat(plot->itemToInfo(Middleline), true);
            showCurveNoReploat(plot->itemToInfo(AcuracyPipeUpLine), true);
            showCurveNoReploat(plot->itemToInfo(AcuracyPipeDownLine), true);
        }
        if (!vError_points.isEmpty())
        {
            // выключим курву частоты, но включим погрешность
            if (filterFError->isChecked())
            {
                int i, size = vError_points.size();
                QVector<float> t(size);
                for (i = 0; i < size; ++i)
                    t[i] = vError_points.at(i).y();
                t = filterDataArray(t,
                                    SettingsCmdLine::settings->value(
                                        "Measure/FreqErrorFilterWindow").toInt());
                QVector<QPointF> data(size);
                for (i = 0; i < size; ++i)
                    data[i] = QPointF(vError_points.at(i).x(), t.at(i));
                FreqErrorCurve->setSamples(data);
            }
            else
                FreqErrorCurve->setSamples(vError_points);
            showCurveNoReploat(plot->itemToInfo(FreqErrorCurve), true);
            showCurveNoReploat(plot->itemToInfo(F_P_Curve), false);
        }
        break;
    }
}

QVector<QPointF> GSTGraph::SortPoints(const QVector<QPointF> &input)
{
    QVector<QPointF> Data(input); //result

    QMutableVectorIterator<QPointF> it(Data);
    bool f = true;
    while (f)
    {
        f = false;
        it.toFront();
        while (true)
        {
            float a = it.next().x(); // ВОЗВРАТИТ !!ТЕКУЩИЙ!! и ПОТОМ ПЕРЕЙДЕТ
            if (!it.hasNext())
                break;
            float b = it.peekNext().x(); // здесь бедет следующий после того, который верент it.value(), тоесть как-раз СЛЕДУЮЩИЙ (Бред сивой кобыли, ага)
            if (b < a)
            {
                QPointF t_F = it.value(); // таки тут быдет то что под a
                // все верно дальше
                it.setValue(it.peekNext());
                it.next();
                it.setValue(t_F);
                it.previous();
                f = true;
            }
        }
    }
    return Data;
}

QVector<float> GSTGraph::filterDataArray(const QVector<float>& inData, int windowLen)
{
    SlideMiddleFilter filter;
    filter.setFilterLen(windowLen);
    QVector<float> res(inData.size());
    for (int i = 0; i < inData.size(); ++i)
        res[i] = filter.addValue(inData.at(i));
    return res;
}

void GSTGraph::saveDefaultHistoryLen(int len)
{
    if (len >= MINIMAL_HISTORY_LEN)
        (*SettingsCmdLine::settings)["Graph/HistoryLen"] = len;
}

void GSTGraph::saveGraphData()
{
    QString filename = QFileDialog::getSaveFileName(this, trUtf8("Сохранить данные как..."),
                                                    QDir::currentPath() + "/" + plot->title().text() + ".csv", trUtf8("CSV table (*.csv)")
                                                #ifndef WIN32
                                                    , NULL, QFileDialog::DontUseNativeDialog
                                                #endif
                                                    );
    if (filename.isEmpty())
        return;
    if (!filename.endsWith(trUtf8(".csv")))
        filename.append(trUtf8(".csv"));
    QFile dataFile(filename);
    QByteArray t;
    if (!dataFile.open(QIODevice::WriteOnly))
    {
        LOG_ERROR(trUtf8("Failed to open file %1").arg(dataFile.fileName()));
        return;
    }

    switch(CurentMode)
    {
    case Rk_P_F_OF_T:
    {
        QString str = trUtf8("Date;P;F [Hz];Rk [kOm]\n");
        t.append(str);
        dataFile.write(t);
        for (int i = 0; i < Vpreasure_T_Curve.size(); ++i)
        {
            str = trUtf8("%1;%2;%3;%4 \n").arg(
                        QTime().addMSecs(Vpreasure_T_Curve.at(i).x()).toString("hh:mm:ss.zzz")).arg(
                        Vpreasure_T_Curve.at(i).y(), 0, 'f', 3).arg(
                        VF_T_Curve.at(i).y(), 0, 'f', 3).arg(
                        VRk_T_Curve.at(i).y(), 0, 'f', 3);
            t.clear();
            t.append(str);
            dataFile.write(t);
        }
        LOG_INFO(trUtf8("Linear reading data saved to: %1").arg(dataFile.fileName()));
        unsaved = false;
    }
        break;
    case RK_F_OF_P:
    {
        QTextCodec *codec = QTextCodec::codecForName("Windows-1251");
        QString str = trUtf8("Отчёт %1\nДавление;Rk;F\n").arg(QDateTime::currentDateTime().toString("dd.mm.yyyy hh:mm:ss"));
        t = codec->fromUnicode(str);
        dataFile.write(t);
        for (int i = 0; i < VRk_P_Curve.size(); ++i)
        {
            str = trUtf8("%1;%2;%3 \n").arg(
                        VF_P_Curve.at(i).x(), 0, 'f', 3).arg(
                        VRk_P_Curve.at(i).y(), 0, 'f', 3).arg(
                        VF_P_Curve.at(i).y(), 0, 'f', 3);
            t.clear();
            t.append(str);
            dataFile.write(t);
        }
        LOG_INFO(trUtf8("Rk(P) data saved to: %1").arg(dataFile.fileName()));
        unsaved = false;
    }
        break;
    default:
        break;
    }
    dataFile.close();
}

void GSTGraph::saveGraphImage()
{
    if (onlylastMeasures->isChecked() && CurentMode == RK_F_OF_P)
    {
        // переключиться на полный график
        onlylastMeasures->setChecked(false);
        plotAll();
        plot->replot();
    }

    QString filename = QFileDialog::getSaveFileName(this, trUtf8("Сохранить график как..."),
                                                    QDir::currentPath()  + "/" + plot->title().text() + ".png",
                                                    trUtf8("PNG (*.png)")
                                                #ifndef WIN32
                                                    , NULL, QFileDialog::DontUseNativeDialog
                                                #endif
                                                    );
    if (filename.isEmpty())
        return;
    if (!filename.endsWith(trUtf8(".png")))
        filename.append(trUtf8(".png"));
    if (!filename.isEmpty())
    {
        QFile picture(filename);
        if (!picture.open(QIODevice::WriteOnly))
        {
            LOG_ERROR(trUtf8("Failed to open file %1").arg(picture.fileName()));
            return;
        }
        QPixmap graphData = QPixmap::grabWidget(plot);
        graphData.save(&picture, "PNG");
        unsaved = false;
        picture.close();
    }
}

void GSTGraph::printGraph()
{
    QPrinter printer;
    QPrintDialog PrinterDialog (&printer, this);
    if (PrinterDialog.exec() == QDialog::Accepted)
    {
        printer.setColorMode(QPrinter::GrayScale);
        printer.setOrientation(QPrinter::Landscape);
        QwtPlotRenderer plotRenderer;
        QPainter painter;

        plotRenderer.setDiscardFlag(QwtPlotRenderer::DiscardBackground);
        plotRenderer.setDiscardFlag(QwtPlotRenderer::DiscardCanvasBackground);
        plotRenderer.setDiscardFlag(QwtPlotRenderer::DiscardCanvasFrame);

        painter.begin(&printer);
        plotRenderer.render(plot, &painter, painter.viewport());
        painter.end();

        unsaved = false;
    }
}

QPoint GSTGraph::FindMinMaxIndexes(const QVector<double> &data, double minMin_MaxSpace)
{
    int IMin, IMax;
    double Max = std::numeric_limits<double>::min();
    double Min = std::numeric_limits<double>::max();
    QVector<double>::ConstIterator it = data.begin();
    QVector<double>::ConstIterator it_end = data.end();
    for (int i = 0; it < it_end; ++it, ++i)
    {
        if (*it > Max)
        {
            Max = *it;
            IMax = i;
        }
        if (*it < Min)
        {
            Min = *it;
            IMin = i;
        }
    }
    if (Min == 0.0)
        return Max > minMin_MaxSpace ? QPoint(IMax, IMax) : QPoint(0, 0);
    return (Max - Min) > minMin_MaxSpace ? QPoint(IMax, IMin) :
                                           QPoint(0, 0);
}


bool GSTGraph::hasUnsavedData() const
{
    return unsaved;
}

void GSTGraph::resetUnsaved()
{
    unsaved = false;
}
