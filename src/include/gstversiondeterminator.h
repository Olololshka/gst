#ifndef GSTVERSIONDETERMINATOR_H
#define GSTVERSIONDETERMINATOR_H

#include <QDialog>
#include <stdint.h>

#define PREASURE_SENSOR_ID      (0xDB00)

#define DEVICES_ID_ADRESS       (0x0000)

#define SCAN_MAX_ADRESS         (0x13)

#define DEFAULT_TIMEOUT         (12000)

#define _FT_VID                 "0403"
#define _FT_PID                 "6001"
#define WIN32_ZVERCD_KOSTIL     "FTDIBUS\\COMPORT&VID_0403&PID_6001"

class QPushButton;
class QProgressBar;
class QLabel;
class ModbusEngine;
class ModbusRequest;
class GSTSystem;

class GSTVersionDeterminator : public QDialog
{
    Q_OBJECT
public:
    struct sDetectHelper
    {
        ModbusRequest* request;
        uint16_t properAnsver;
    };

    explicit GSTVersionDeterminator(ModbusEngine *mb, QWidget *parent = 0);

    GSTSystem* getSystem() const;

public slots:
    virtual void reject();

protected:
    virtual void showEvent(QShowEvent * event);

private:
    GSTSystem* gstSystem;

    QLabel* Infostring;
    QProgressBar *progressBar;
    QPushButton* cancelButton;

    ModbusEngine *mb;

    bool fCancel;

    uint16_t deviceIDAtAdress(uint8_t adress);

private slots:
    void StartScan();
};

#endif // GSTVERSIONDETERMINATOR_H
