#ifndef PREASURESTATE_H
#define PREASURESTATE_H

#include <QObject>

#include "slidemiddlefilter.h"

#include <QVector>

class PreasureState : public QObject, public SlideMiddleFilter
{
    Q_OBJECT
public:
    enum PreasureStates
    {
        IDLE = 0,
        STABLE = 1 << 0,
        FALLING = 1 << 1,
        UPING = 1 << 2,
        MORE_ATM = 1 << 3,
        NEAR_ATM = 1 << 4,
        LESS_ATM = 1 << 5,
        VACUUM = 1 << 6,
        MAXIMAL = 1 << 7
    };

    explicit PreasureState(QObject *parent = 0);

    void SetMaxPreasure(float maxPreasure);
    void SetVacuum(float vacuum);

    void SetAtmosphere(float AtmPreasure);
    void SetAtmosphereSigma(float AtmSigma);
    void SetStableMaxDelta(float stableMaxDelta);

    PreasureStates getState() const;

    virtual float addValue(float newVal);

signals:
    void StateChanged(PreasureState::PreasureStates newstate);

private:
    void setState(PreasureState::PreasureStates newstate);
    QString enPreasureStates2Text(PreasureStates state);
    PreasureStates DetectState(float oldest, float middle, float current);

    PreasureStates curentState;

    float MaxPreasure;
    float VacuumPreasure;

    float AtmospherePreasure;
    float AtmosphereSigma;
    float StableMaxDelta;
};

#endif // PREASURESTATE_H
