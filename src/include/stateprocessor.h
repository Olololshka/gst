#ifndef STATEPROCESSOR_H
#define STATEPROCESSOR_H

#include <QObject>
#include <QTimer>

#include "preasurestate.h"

// реверса не будет, нельзя давать авкуумному насосу давление > ATM

class StateProcessor : public QObject
{
    Q_OBJECT
public:
    enum valveReq
    {
        CLOSE_ALL,
        OPEN_VACUUM,
        OPEN_ATM,
        OPEN_PREASURE
    };

    enum States
    {
        IDLE,
        DETECTING_STATE,
        MORE_THEN_ATM,
        ATM,
        LESS_THAN_ATM,
        VACUUM,
        PREASURE,
        IN_PROGRESS,
        REQUEST,
        READY
    };

    explicit StateProcessor(QObject *parent = 0);

    void Start();
    void Stop();
    
    void UpdateState(PreasureState::PreasureStates newState, float newPreasure);
    bool setWorkRange(float Min, float Max);
    void setAtmospherePreasure(float p);

signals:
    void progress(const QString& desc, int present);
    void ValveRequest(const QString& desc, StateProcessor::valveReq req);
    void Interrupted();
    void SwitchDisplayModeReq(bool Rk_P_mode);
    void MeasureFinished();

private:
    QTimer timer;
    States CurentState;

    PreasureState::PreasureStates preasureState;
    PreasureState::PreasureStates requestedAction;
    float curentPreasure;

    float startPreasure, endpreasure, atmospherePreasure;
    float StageStartPreasure;

    int tryCounter;
    bool lastMode;

    int toPersent(float min, float max, float value);
    void DisplayMeasureRk(bool mode);

    int CalculeTrysCount();

    QString LastRequestStr;
    valveReq lastValveReq;

    void requestPreasureChange(const QString &requestInfo, PreasureState::PreasureStates action, valveReq ValveReq);
};

#endif // STATEPROCESSOR_H
