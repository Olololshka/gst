#ifndef GSTSYSTEM_H
#define GSTSYSTEM_H

#include <QObject>
#include <QList>
#include <stdint.h>

class GST;
class DB00;
class KlapanSwitcher;
class QBitArray;

class ModbusEngine;

class GSTSystem : public QObject
{
    Q_OBJECT
public:
    enum enGSTVersion
    {
        UNSUPPORTED = 0,
        GST1_1S = 1,
        GST1_2S = 2,
        GST3_1S_1K = 3
    };

    struct GSTData
    {
        float Rk;
        float F;
    } ;

    explicit GSTSystem(ModbusEngine* mb, QObject *parent = 0);

    void addGST(const uint8_t addr);
    void addSensor(const uint8_t addr);
    void AddKlapans(const uint8_t addr);

    QList<GST*> getGST() const;
    QList<DB00*> getSensors() const;
    QList<KlapanSwitcher*> getKlapans() const;

    bool TestMinimalRequred();
    void removeUnconnectedDevices();
    void SaveConfiguration();

    bool setActiveSensor(uint8_t addres);
    bool setActiveKlapanCtrl(uint8_t addres);

    unsigned char getAllGSTData(GSTData* pOutData) const;

    float getPreasure(bool *ok = NULL);

    bool switchKlapan(uint16_t klapan, bool newstate);
    bool closeAllKlapans();

    QBitArray getInputs(bool *ok = NULL);

private:
    QList<GST*> lGst;
    QList<DB00*> lSensors;
    QList<KlapanSwitcher*> lKlapans;

    DB00* ActiveSensor;
    KlapanSwitcher* ActiveKlapan;

    ModbusEngine* mb;
};

#endif // GSTSYSTEM_H
