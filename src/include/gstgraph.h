#ifndef GSTGRAPH_H
#define GSTGRAPH_H

#include <QWidget>
#include <qwt_plot.h>
#include <QTime>
#include <QVector>
#include <QPointF>

#define MINIMAL_HISTORY_LEN (10)

class QwtPlotCurve;
class QwtScaleDraw;
class QSpinBox;
class QCheckBox;
class QwtLegend;
class QLabel;
class QwtPlotTextLabel;

class GSTGraph : public QWidget
{
    Q_OBJECT
public:
    enum GraphMode
    {
        Rk_P_F_OF_T,
        RK_F_OF_P
    };

    GSTGraph(QWidget *parent = NULL);
    virtual ~GSTGraph();

    void f(bool wasSwap);

    bool hasUnsavedData() const;
    void resetUnsaved();

public slots:
    void addData(float P, float Rk, float F, QTime timestamp = QTime::currentTime());
    void setMode(GraphMode mode);

    void SetHistoryLen(int len);

    void clean();

    virtual void replot();

    void showCustomRk_P_graph(const QVector<QPointF> &Rk_Data, const QVector<QPointF> &F_Data);
    void BuildRkAccuracyPipe();
    void BuildFErrorCurve();
    void setGraphTitle(const QString& title);

    void AcceptNewSettings();

    void saveGraphData();
    void saveGraphImage();
    void printGraph();

private:
    QwtPlot *plot;

    QwtPlotTextLabel *label_sensitiveCoeff;

    QwtScaleDraw* scaleDrawTime;
    QwtScaleDraw* scaleDrawDefault;

    QwtLegend *legend;
    QwtPlotCurve *preasure_T_Curve, *Rk_T_Curve, *FTCurve;
    QwtPlotCurve *Rk_P_Curve, *F_P_Curve;

    QwtPlotCurve *Middleline, *AcuracyPipeUpLine, *AcuracyPipeDownLine;

    QwtPlotCurve *FreqErrorCurve;

    QVector<QPointF> Vpreasure_T_Curve, VRk_T_Curve, VF_T_Curve;
    QVector<QPointF> VRk_P_Curve;
    QVector<QPointF> VF_P_Curve;
    QVector<QPointF> VMiddleline;
    QVector<QPointF> vError_points;

    GraphMode CurentMode;

    QSpinBox* numofLastOnes;
    QCheckBox* onlylastMeasures, *filterFError;
    QLabel* labelPreasureChengeSpeed;

    bool fShowLasthistory;

    QAction *AClear, *ASaveData, *ASavePicture, *APrintGraph, *ADisplayFragment;

    void plotAll();

    static QVector<QPointF> SortPoints(const QVector<QPointF>& input);

    QVector<float> filterDataArray(const QVector<float> &inData, int windowLen);

    bool unsaved;

private slots:
    void saveDefaultHistoryLen(int len);
    void showCurve(QVariant iteminfo, bool on, int index);
    void showCurveNoReploat(QVariant iteminfo, bool on);
    void ShowFragment(bool show);

    static QPoint FindMinMaxIndexes(const QVector<double>& data, double minMin_MaxSpace);
};

#endif // GSTGRAPH_H
