#ifndef MAINFORM_H
#define MAINFORM_H

#include <QMainWindow>
#include <QList>
#include <QTimer>

#include "preasurestate.h"
#include "stateprocessor.h"

#define DEFAULT_START_PREASURE      (10)
#define DEFAULT_END_PREASURE        (6 * 735)
#define MAX_PREASURE                (16 * 735)

#define KLAPAN_EXEC_OPERATION_TRYS  (5)
#define DEFAULT_ATM_KLAPAN          (0)
#define DEFAULT_VACUUM_KLAPAN       (1)
#define DEFAULT_PREASURE_KLAPAN     (2)

#define WINDOW_DEF_H                (1024)
#define WINDOW_DEF_W                (600)

class QAction;
class GSTSystem;
class GSTGraph;
class QShowEvent;
class QTabWidget;
class QPushButton;
class QProgressBar;
class QLabel;
class QGroupBox;
class QDoubleSpinBox;
class QMessageBox;

class mainform : public QMainWindow
{
    Q_OBJECT
public:

    explicit mainform(GSTSystem* gstSystem, QWidget *parent = 0);
    virtual ~mainform();
    
private:
    GSTSystem* gstSystem;

    QList<QAction*> SensorsList;
    QList<QAction*> KlapansList;

    QAction* AOptions;
    QAction* OpenExistingGraph;

    QList<GSTGraph*> graphs;

    QTabWidget* tabedWindow;

    QPushButton* LinearReadBtn;
    QPushButton* RkMeasureBth;

    QProgressBar* Progress;
    QLabel* progressDesc;

    QGroupBox* linearReadgrp;
    QGroupBox* MeasureRK;

    QDoubleSpinBox* startPreasure;
    QDoubleSpinBox* endPreasure;

    PreasureState* preasureState;

    StateProcessor* preasureStateProcessor;

    QList<QAction*> klapansButtons;

    QMessageBox* finishPreasureDownDialog;

    QTimer timerPreasureDown;
    int counterPreasureDown;

protected:
    void showEvent(QShowEvent * _e);
    void closeEvent(QCloseEvent * _e);

private slots:
    void selectActiveSensor(bool trigger);
    void selectActiveKlapan(bool trigger);
    void StartLinearRead(bool startStop);
    void StartMeasureRk(bool startstop);
    void timerSlot();

    void UpdateProgress(const QString& desc, int present);
    void ProcessValve(const QString& desc, StateProcessor::valveReq req);
    void SwitchDisplayMode(bool _P);
    void measureFinish();
    void MeasureRKInterrupted();
    void manualKlapanControlSlot(bool trigger);
    void timerPreasureDownSlot();
    void CallOptionsWindow();
    void OpenExistingGraphSlot();

private:
    void showGstGraphs(bool isTabbed = false);
    QAction* getKlapanAction(int n);
    void blockKlapansActions(bool block);

    void refreshSettings();

    QTimer timer;
    bool blockAddMeasures;

signals:
    void settingsChanged();
};

#endif // MAINFORM_H
