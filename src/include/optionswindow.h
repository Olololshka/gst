#ifndef OPTIONSWINDOW_H
#define OPTIONSWINDOW_H

#include <QDialog>

#define DEFAULT_MEASURE_INTERVAL        (300)
#define DEFAULT_ATMOSPHERE_PREASURE     (747.0)
#define DEFAULT_ATMOSPHERE_TOLERANCE    (20.0)
#define DEFAULT_MAX_STABLE_DELTA        (10.0)
#define DEFAULT_RK_ACURACY_PIPE_RADIUS  (15)
#define DEFAULT_HYSTO_INTERVALS_NUM     (7)
#define DEFAULT_POLY_POW                (1)
#define DEFAULT_DROP_LAST_POINTS_NUM    (0)
#define DEFAULT_MAX_SENSIVITY           (10)
#define DEFAULT_ENABLE_P_HI_TO_LO       (false)
#define DEFAULT_PREASURE_UNITS_NAME     "mm Hg"
#define DEFAULT_UI_LAYOUT               "windows"
#define DEFAULT_ACTION_WAIT_TIME        (8)

class QPushButton;
class QSpinBox;
class QComboBox;
class QDoubleSpinBox;
class QCheckBox;
class QLineEdit;
class QLabel;

class OptionsWindow : public QDialog
{
    Q_OBJECT
public:
    explicit OptionsWindow(QWidget *parent = 0);

public slots:
    virtual void accept();

private:
    QPushButton *okButton, *cancelButton, *resetButton;
    QSpinBox *updateInterval, *HystogrammIntervals, *PolyPow, *ActionAcceptInterval, *DropLastPoints;
    QDoubleSpinBox *Atm, *AtmTolerance, *stableDelta, *RkAcuracy, *MaxSensivity;
    QComboBox* windowStyle;
    QCheckBox* enablePHi2Lo;
    QLineEdit* PreasureUnits;

    QLabel* lb[4];

private slots:
    void resetToDefaults();
    void rewriteLabels(const QString& val);
};

#endif // OPTIONSWINDOW_H
