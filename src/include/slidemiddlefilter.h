#ifndef SLIDEMIDDLEFILTER_H
#define SLIDEMIDDLEFILTER_H

#include <QVector>

class SlideMiddleFilter
{
public:
    SlideMiddleFilter();

    void setFilterLen(unsigned int len);

    virtual float addValue(float newVal);
    float getFilterValue() const;

    void reset();

    float oldest();
    float middle();
    float last();

    unsigned int getfilterLen() const;
    unsigned int getfillFilterCounter() const;

private:
    unsigned int filterLen;
    unsigned int fillFilterCounter;

    QVector<float> rawData;
    unsigned int rawDataWritePos;
    QVector<float> filtredValues;
    unsigned int filtredValuesWritePos;
};

#endif // SLIDEMIDDLEFILTER_H
