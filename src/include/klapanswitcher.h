#ifndef KLAPANSWITCHER_H
#define KLAPANSWITCHER_H

#include <QObject>
#include <stdint.h>
#include <QBitArray>

#define KLAPAN_CONTROLLER_ID            (0xDBFD)

#define KLAPAN_KLAPANS_START_ADRESS     (0)
#define KLAPAN_NKLAPANS                 (8)
#define KLAPAN_INPUTS_START_ADRESS      (0)
#define KLAPAN_NINPUTS                  (4)
#define KLAPAN_MINIMAL_RETRY_INTERVAL   (5000)

#define KLAPAN_ACCEPT_BIT_ADDR          (9)

class ModbusEngine;
class ModbusRequest;

class KlapanSwitcher : public QObject
{
    Q_OBJECT
public:
    explicit KlapanSwitcher(ModbusEngine *mb, uint8_t addr, QObject *parent = 0);
    virtual ~KlapanSwitcher();

    uint8_t getAddres() const;

    bool Test() const;

    QBitArray getInputs(bool *ok = NULL);
    QBitArray getCoils(bool *ok = NULL);
    bool switchKlapan(uint16_t number, bool newState);
    bool closeAll();

    void setRetryInterval(int msec);
    void setOnlySingleKlapanProtect(bool on);

protected:
    virtual void timerEvent(QTimerEvent * event);

private:
    ModbusEngine* mb;
    uint8_t addr;
    ModbusRequest* readAllCoilsReq;
    ModbusRequest* readAllInputsReq;
    ModbusRequest* writeAcceptBitReq;

    int timerID;

    QByteArray KlapanStateForRefresh;

    bool singleKlapanProtect;
    bool acceptNewState();

    bool llRequestCall(ModbusRequest* req);
};

#endif // KLAPANSWITCHER_H
