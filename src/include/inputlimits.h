#ifndef INPUTLIMITS_H
#define INPUTLIMITS_H

#include <QDialog>

class QDoubleSpinBox;

class inputLimits : public QDialog
{
    Q_OBJECT
public:
    explicit inputLimits(const int Min, const int Max, QWidget *parent = 0);
    
    float from() const;
    float to() const;

private slots:
    void Validate();

private:
    QDoubleSpinBox *sbFrom, * sbTo;
};

#endif // INPUTLIMITS_H
