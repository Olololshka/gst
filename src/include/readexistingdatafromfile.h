#ifndef READEXISTINGDATAFROMFILE_H
#define READEXISTINGDATAFROMFILE_H

#include <QString>

class GSTGraph;

class ReadExistingDataFromFile
{
public:    
    static GSTGraph* OpenExistingData(const QString& filename);
};

#endif // READEXISTINGDATAFROMFILE_H
