#ifndef GST_H
#define GST_H

#include <QObject>
#include <stdint.h>

#define GST_ID                  (0xDB01)

#define GST_DATA_START_ADRESS   (0x0002)

class ModbusEngine;
class ModbusRequest;

class GST : public QObject
{
    Q_OBJECT
public:
    explicit GST(ModbusEngine* mb, uint8_t addr, QObject *parent = 0);
    virtual ~GST();

    uint8_t getAddres() const;

    bool Test() const;

    QList<float> getOutputValues(bool *ok);
    
private:
    ModbusEngine* mb;
    uint8_t addr;
    ModbusRequest* dataRequest;
};

#endif // GST_H
