#include <qmodbus/ModbusRequest.h>
#include <qmodbus/ModbusEngine.h>
#include <mylog/mylog.h>

#include "gst.h"

GST::GST(ModbusEngine* mb, uint8_t addr, QObject *parent) :
    QObject(parent)
{
    this->mb = mb;
    this->addr = addr;

    dataRequest = ModbusRequest::BuildReadInputsRequest(addr, GST_DATA_START_ADRESS, 2 * sizeof(float) / sizeof(uint16_t));
}

GST::~GST()
{
    dataRequest->deleteLater();
}

uint8_t GST::getAddres() const
{
    return addr;
}

bool GST::Test() const
{
    ModbusRequest* testreq = ModbusRequest::BuildReadHoldingRequest(addr, 0);
    bool res = false;
    mb->SyncRequest(*testreq);
    if (testreq->getErrorrCode() == ModbusRequest::ERR_OK)
        if (testreq->getAnsverAsShort().at(0) == GST_ID)
            res = true;
    testreq->deleteLater();
    return res;
}

QList<float> GST::getOutputValues(bool *ok)
{
    mb->SyncRequest(*dataRequest);
    if (dataRequest->getErrorrCode() != ModbusRequest::ERR_OK)
    {
        if (ok != NULL)
            *ok = false;
        return QList<float>();
    }
    else
    {
        if (ok != NULL)
            *ok = true;
        return dataRequest->getAnsverAsFloat();
    }
}

