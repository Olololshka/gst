#include "slidemiddlefilter.h"

SlideMiddleFilter::SlideMiddleFilter()
{
    filterLen = 0;
    reset();
}

void SlideMiddleFilter::setFilterLen(unsigned int len)
{
    filterLen = len;
    reset();
}

float SlideMiddleFilter::addValue(float newVal)
{
    if (fillFilterCounter < filterLen)
    {
        for (int i = rawDataWritePos; i < filterLen + 1; ++i)
            rawData[i] = newVal;
        ++rawDataWritePos;
    }
    else
    {
        rawData[rawDataWritePos++] = newVal;
        if (rawDataWritePos > filterLen)
            rawDataWritePos = 0; // начать сначала
    }

    float res = 0;

    for (int i = 0; i < filterLen + 1; ++i)
        res += rawData[i];
    res /= filterLen + 1;
    if (fillFilterCounter < filterLen)
    {
        for (int i = filtredValuesWritePos; i < filterLen + 1; ++i)
            filtredValues[i] = res;
        ++filtredValuesWritePos;
        ++fillFilterCounter;
    }
    else
    {
        filtredValues[filtredValuesWritePos++] = res;
        if (filtredValuesWritePos > filterLen)
            filtredValuesWritePos = 0;
    }
    return res;
}

float SlideMiddleFilter::getFilterValue() const
{
    return ((signed long)filtredValuesWritePos - 1 < 0) ?
                (filtredValues[filterLen]) : (filtredValues[filtredValuesWritePos - 1]);
}

void SlideMiddleFilter::reset()
{
    rawData = QVector<float>(filterLen + 1);
    filtredValues = QVector<float>(filterLen + 1);

    rawDataWritePos = 0;
    filtredValuesWritePos = 0;
    fillFilterCounter = 0;
}

float SlideMiddleFilter::oldest()
{
    return filtredValues.at(filtredValuesWritePos);
}

float SlideMiddleFilter::middle()
{
    int middlepos = filtredValuesWritePos - filterLen / 2;
    return filtredValues.at(middlepos < 0 ? (filterLen + middlepos) : (middlepos));
}

float SlideMiddleFilter::last()
{
    if (filtredValuesWritePos == 0)
        return filtredValues.last();
    else
        return filtredValues.at(filtredValuesWritePos - 1);
}

unsigned int SlideMiddleFilter::getfilterLen() const
{
    return filterLen;
}

unsigned int SlideMiddleFilter::getfillFilterCounter() const
{
    return fillFilterCounter;
}




