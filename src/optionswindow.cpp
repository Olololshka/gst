#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <QCheckBox>
#include <QLineEdit>

#include <settingscmdline/settingscmdline.h>
#include <mylog/mylog.h>

#include "optionswindow.h"

static const char* uiLayoutNames[] =
{
    "windows", "tabs"
};

static const char* parametersDesc[] =
{
    "Атмосферное давление [%1]",
    "Допустимое отклонение атмосферного давления [%1]",
    "Точность определения стабильности давления [%1]",
    "Максимально-возможная чувствительность [Гц/%1]"
};

OptionsWindow::OptionsWindow(QWidget *parent) :
    QDialog(parent)
{
    okButton = new QPushButton(trUtf8("Ok"));
    connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));
    cancelButton = new QPushButton(trUtf8("Отмена"));
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
    resetButton = new QPushButton(trUtf8("Сброс"));
    connect(resetButton, SIGNAL(clicked()), this, SLOT(resetToDefaults()));

    QHBoxLayout* btnLayout = new QHBoxLayout;
    btnLayout->addWidget(okButton);
    btnLayout->addWidget(resetButton);
    btnLayout->addWidget(cancelButton);

    QString pU = SettingsCmdLine::settings->value("Measure/PreasureUnitsName").toString();

    QGridLayout *grid = new QGridLayout;
    QLabel* label;
    grid->addWidget(label =new QLabel(trUtf8("Интервал опроса [мс]")), 0, 0);
    label->setToolTip(trUtf8("Изменяет скорость опроса датчиков"));
    grid->addWidget(label = lb[0] = new QLabel(trUtf8(parametersDesc[0]).arg(pU)), 1, 0);
    label->setToolTip(trUtf8("Требуется для определения состояния, возможно возникновение ошибки при\nсерьезном отклонении фактического значения от установленого\nУстановите это значение в единицах Вашего датчика"));
    grid->addWidget(label = lb[1] = new QLabel(trUtf8(parametersDesc[1]).arg(pU)), 2, 0);
    label->setToolTip(trUtf8("Считать камеру открытой, если фактическое давление в ней укладывается в интервал [Pатм]±<Это значение>\nУстановите это значение в единицах Вашего датчика"));
    grid->addWidget(label = lb[2] = new QLabel(trUtf8(parametersDesc[2]).arg(pU)), 3, 0);
    label->setToolTip(trUtf8("Даление считается стабильным, если за 10 измерений оно изменяется не более чем на эту величину\nУстановите это значение в единицах Вашего датчика"));
    grid->addWidget(label = new QLabel(trUtf8("Допустимое отклонение величины Rk [%]")), 4, 0);
    label->setToolTip(trUtf8("Устанавливает размер трубки точности от среднего значения Rk при построении графика Rk(P)"));
    grid->addWidget(label = new QLabel(trUtf8("Количество поддиапазонов гистограммы")), 5, 0);
    label->setToolTip(trUtf8("Используется в процессе оценки среднего значения Rk гистрограмным методом"));
    grid->addWidget(label = new QLabel(trUtf8("Порядок апроксимирующего полинома F(P)")), 6, 0);
    label->setToolTip(trUtf8("Задает степень полинома, апроксимирующего значения F(P),\nиспользуется при построении графика отклонения значений частоты от и определения коэфициента чувствительности резонатора"));
    grid->addWidget(label = new QLabel(trUtf8("Отбрасывать конечные точки [шт]")), 7, 0);
    label->setToolTip(trUtf8("Отбросить указанное количество точек на конце графика при построении графика отклонения частоты от прогнозируемой"));
    grid->addWidget(label = lb[3] = new QLabel(trUtf8(parametersDesc[3]).arg(pU)), 8, 0);
    label->setToolTip(trUtf8("Задайте максимально-возможный коэфициент чувствительности резонатора, для отбрасывания фрагментов срыва генерации"));
    grid->addWidget(label = new QLabel(trUtf8("Расположение окон")), 9, 0);
    label->setToolTip(trUtf8("Задает вид интерфейса, если подключено более одного ГСТ"));
    grid->addWidget(label = new QLabel(trUtf8("Интервал ожидания подтверждения выполния выполнения [c]")), 10, 0);
    label->setToolTip(trUtf8("Задет интервал ожидания выполнения пользователем запрошеных дайствий по изменению давления в камере"));
    grid->addWidget(label = new QLabel(trUtf8("Разрешить работу от высокого давления к низкому")), 11, 0);
    label->setToolTip(trUtf8("Разрешает работу устройства при изменение давления от высокого к низкому,\nпозаботесь о безопастности вашего вакуумного оборудования, если хотитие использовать\nэтот режим"));
    grid->addWidget(label = new QLabel(trUtf8("Единицы измерения давления")), 12, 0);
    label->setToolTip(trUtf8("Едицы измерения, для отображения подписей осей и полей ввода."));

    grid->addWidget(updateInterval = new QSpinBox, 0, 1);
    grid->addWidget(Atm = new QDoubleSpinBox, 1, 1);
    grid->addWidget(AtmTolerance = new QDoubleSpinBox, 2, 1);
    grid->addWidget(stableDelta = new QDoubleSpinBox, 3, 1);
    grid->addWidget(RkAcuracy = new QDoubleSpinBox, 4, 1);
    grid->addWidget(HystogrammIntervals = new QSpinBox, 5, 1);
    grid->addWidget(PolyPow = new QSpinBox, 6, 1);
    grid->addWidget(DropLastPoints = new QSpinBox, 7, 1);
    grid->addWidget(MaxSensivity = new QDoubleSpinBox, 8, 1);
    grid->addWidget(windowStyle = new QComboBox, 9, 1);
    grid->addWidget(ActionAcceptInterval = new QSpinBox, 10, 1);
    grid->addWidget(enablePHi2Lo = new QCheckBox, 11, 1);
    grid->addWidget(PreasureUnits = new QLineEdit, 12, 1);

    // read
    updateInterval->setMinimum(10);
    updateInterval->setMaximum(10000);
    updateInterval->setValue(SettingsCmdLine::settings->value("Measure/UpdateInterval_ms").toInt());
    Atm->setMinimum(0.1);
    Atm->setMaximum(10000);
    Atm->setSingleStep(0.1);
    Atm->setValue(SettingsCmdLine::settings->value("Measure/AtmospherePreasure").toDouble());
    AtmTolerance->setMinimum(0.1);
    AtmTolerance->setMaximum(10000);
    AtmTolerance->setSingleStep(0.1);
    AtmTolerance->setValue(SettingsCmdLine::settings->value("Measure/AtmosphereTolerancee").toDouble());
    stableDelta->setMinimum(0.01);
    stableDelta->setMaximum(1000);
    stableDelta->setSingleStep(0.01);
    stableDelta->setValue(SettingsCmdLine::settings->value("Measure/MaxStableDelta").toDouble());
    RkAcuracy->setMinimum(1.0);
    RkAcuracy->setMaximum(50);
    RkAcuracy->setSingleStep(0.1);
    RkAcuracy->setValue(SettingsCmdLine::settings->value("Measure/AcuracyPipe_pr").toDouble());
    HystogrammIntervals->setMinimum(5);
    HystogrammIntervals->setMaximum(20);
    HystogrammIntervals->setValue(SettingsCmdLine::settings->value("Measure/HystogramFields").toDouble());
    PolyPow->setMinimum(1);
    PolyPow->setMaximum(4);
    PolyPow->setValue(SettingsCmdLine::settings->value("Measure/AproximatePolyPow").toDouble());
    DropLastPoints->setMinimum(0);
    DropLastPoints->setMaximum(50);
    DropLastPoints->setValue(SettingsCmdLine::settings->value("Measure/DropLastPoints").toInt());
    MaxSensivity->setMinimum(0.1);
    MaxSensivity->setMaximum(50);
    MaxSensivity->setValue(SettingsCmdLine::settings->value("Measure/maxFreqChangeSpeed").toDouble());
    windowStyle->addItem(trUtf8("Отдельные окна"));
    windowStyle->addItem(trUtf8("Закладки"));
    windowStyle->setCurrentIndex(SettingsCmdLine::settings->value("Interface/layout").toString() == trUtf8(uiLayoutNames[0]) ? (0) : (1));
    ActionAcceptInterval->setMinimum(2);
    ActionAcceptInterval->setMaximum(15);
    ActionAcceptInterval->setValue(SettingsCmdLine::settings->value("Interface/ActionWaitTime").toInt());
    enablePHi2Lo->setChecked(SettingsCmdLine::settings->value("Measure/EnablePreasureHiToLo").toBool());
    PreasureUnits->setText(SettingsCmdLine::settings->value("Measure/PreasureUnitsName").toString());

    connect(PreasureUnits, SIGNAL(textChanged(QString)), this, SLOT(rewriteLabels(QString)));

    QVBoxLayout* rootlayout = new QVBoxLayout;
    rootlayout->addLayout(grid);
    rootlayout->addLayout(btnLayout);
    setLayout(rootlayout);
}

void OptionsWindow::accept()
{
    // save
    (*SettingsCmdLine::settings)["Measure/UpdateInterval_ms"] = updateInterval->value();
    (*SettingsCmdLine::settings)["Measure/AtmospherePreasure"] = Atm->value();
    (*SettingsCmdLine::settings)["Measure/AtmosphereTolerancee"] = AtmTolerance->value();
    (*SettingsCmdLine::settings)["Measure/MaxStableDelta"] = stableDelta->value();
    (*SettingsCmdLine::settings)["Measure/AcuracyPipe_pr"] = RkAcuracy->value();
    (*SettingsCmdLine::settings)["Measure/HystogramFields"] = HystogrammIntervals->value();
    (*SettingsCmdLine::settings)["Measure/AproximatePolyPow"] = PolyPow->value();
    (*SettingsCmdLine::settings)["Measure/DropLastPoints"] = DropLastPoints->value();
    (*SettingsCmdLine::settings)["Interface/maxFreqChangeSpeed"] = MaxSensivity->value();
    (*SettingsCmdLine::settings)["Interface/layout"] = trUtf8(uiLayoutNames[windowStyle->currentIndex()]);
    (*SettingsCmdLine::settings)["Interface/ActionWaitTime"] = ActionAcceptInterval->value();
    (*SettingsCmdLine::settings)["Measure/EnablePreasureHiToLo"] = enablePHi2Lo->isChecked();
    (*SettingsCmdLine::settings)["Measure/PreasureUnitsName"] = PreasureUnits->text();
    QDialog::accept();
}

void OptionsWindow::resetToDefaults()
{
    LOG_INFO("Reseting settings to default values");
    updateInterval->setValue(DEFAULT_MEASURE_INTERVAL);
    Atm->setValue(DEFAULT_ATMOSPHERE_PREASURE);
    AtmTolerance->setValue(DEFAULT_ATMOSPHERE_TOLERANCE);
    stableDelta->setValue(DEFAULT_MAX_STABLE_DELTA);
    RkAcuracy->setValue(DEFAULT_RK_ACURACY_PIPE_RADIUS);
    HystogrammIntervals->setValue(DEFAULT_HYSTO_INTERVALS_NUM);
    PolyPow->setValue(DEFAULT_POLY_POW);
    DropLastPoints->setValue(DEFAULT_DROP_LAST_POINTS_NUM);
    MaxSensivity->setValue(DEFAULT_MAX_SENSIVITY);
    windowStyle->setCurrentIndex(trUtf8(DEFAULT_UI_LAYOUT) == trUtf8(uiLayoutNames[0]) ? (0) : (1));
    ActionAcceptInterval->setValue(DEFAULT_ACTION_WAIT_TIME);
    enablePHi2Lo->setChecked(DEFAULT_ENABLE_P_HI_TO_LO);
    PreasureUnits->setText(trUtf8(DEFAULT_PREASURE_UNITS_NAME));
}

void OptionsWindow::rewriteLabels(const QString &val)
{
    for (int i = 0; i < 4; ++i)
        lb[i]->setText(trUtf8(parametersDesc[i]).arg(val));
}
