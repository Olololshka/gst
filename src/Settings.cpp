#include <cstdio>
#include <iostream>

#include <QIODevice>

#include <mylog/mylog.h>

#include "optionswindow.h"

#include "Settings.h"

#ifdef WIN32
#define _DEFAULT_PORT	"COM1"
#else
#define _DEFAULT_PORT	"/dev/ttyUSB0"
#endif
#define _DEFAULT_SPEED	(38400)

/// Типы ключей
enum enArgs
{
    OPT_HELP,
    OPT_FORCE_RESCAN,
    OPT_OPEN_FILE
};

/// Таблица ключей программы
static CSimpleOpt::SOption g_rgOptions[] =
{
    {
        OPT_HELP, _T("-h"), SO_NONE
    }, /// "-h"
    {
        OPT_HELP, _T("--help"), SO_NONE
    }, /// "--help"
    {
        OPT_FORCE_RESCAN, _T("--forcerescan"), SO_NONE
    }, /// "--forcerescan"
    {
        OPT_OPEN_FILE, _T("-f"), SO_REQ_SEP
    }, /// "--forcerescan"
    SO_END_OF_OPTIONS
    // END
};

/// Таблица указателей
SettingsCmdLine::pfTable Settings::table =
{
    g_rgOptions,
    Settings::SetDefaultValues,
    Settings::console_ShowUsage,
    Settings::parse_Arg
};

void Settings::console_ShowUsage(cSettingsCmdLine* _this)
{
    std::cout
            << trUtf8(
                   "Usage: GST [options]\n\
\t-f <file>\t Only open specified file, don\'t run mesure module\n\
\t--forcerescan\t Disable use preconfigured data. Enabled by default\n\
\t\t\t You can disable this by change option \n\
\t\t\t \'Global/ForceRescan\' to true in configuration file\n\
\t-h --help\t Display this help\n\
                   \n").toLatin1().data();
}

               struct SettingsCmdLine::key_val_res Settings::parse_Arg(int optCode,
                                                                       const char* optText, char *ArgVal, cSettingsCmdLine* origins)
    {
               SettingsCmdLine::key_val_res result =
    {
            QString(), QVariant(), true
};
switch (optCode)
{
    case OPT_FORCE_RESCAN:
        result.key = "Global/ForceRescan";
        result.val = true;
        break;
    case OPT_OPEN_FILE:
        result.key = "Global/Openonly";
        result.val = QString(ArgVal);
        break;
    default:
        result.res = false;
        break;
}
return result;
}

void Settings::SetDefaultValues(cSettingsCmdLine* _this)
{
    _this->insert("Global/Port", _DEFAULT_PORT);
    _this->insert("Global/Baud", 57600);
    _this->insert("Global/ForceRescan", true); // всеравно история работает плоховато.
    _this->insert("Log/Level", MyLog::LOG_INFO);
    _this->insert("Log/NoFile", false);
    _this->insert("Measure/UpdateInterval_ms", DEFAULT_MEASURE_INTERVAL);
    _this->insert("Measure/AtmospherePreasure", DEFAULT_ATMOSPHERE_PREASURE);
    _this->insert("Measure/AtmosphereTolerancee", DEFAULT_ATMOSPHERE_TOLERANCE);
    _this->insert("Measure/MaxStableDelta", DEFAULT_MAX_STABLE_DELTA);
    _this->insert("Measure/HystogramFields", DEFAULT_HYSTO_INTERVALS_NUM);
    _this->insert("Measure/MinimalPreasureP2PInterval", 0.05); //минимальное расстояние между точками по давлению
    _this->insert("Measure/AcuracyPipe_pr", DEFAULT_RK_ACURACY_PIPE_RADIUS);
    _this->insert("Measure/AproximatePolyPow", DEFAULT_POLY_POW);
    _this->insert("Measure/DropLastPoints", DEFAULT_DROP_LAST_POINTS_NUM);
    _this->insert("Measure/FreqErrorFilterWindow", 5); // длина фильтра скользящего среднего для F  Fapr
    _this->insert("Measure/maxFreqChangeSpeed", DEFAULT_MAX_SENSIVITY); // максимальная скорость изенения частоты, для отрезания срывов генерации
    _this->insert("Measure/EnablePreasureHiToLo", DEFAULT_ENABLE_P_HI_TO_LO); // разрешеине работы по нисходящему давлению
    _this->insert("Measure/PreasureUnitsName", DEFAULT_PREASURE_UNITS_NAME);
    _this->insert("Interface/layout", DEFAULT_UI_LAYOUT);
    _this->insert("Interface/ActionWaitTime", 5); // время ожидания выполнеиния операции
    _this->insert("Global/Timeout", 30); // таймаут I/O [мс]
    _this->insert("Klapans/RefreshKlapanStateDelay_ms", 10000);
}

Settings::Settings(int argc, char *argv[]) :
    QObject(NULL), cSettingsCmdLine(argc, argv, "res/Settings.ini", &table)
  /// параметр 3 устанавливает имя файла настроек программы
{
}

Settings::~Settings()
{
    this->remove("Global/Openonly");
}
