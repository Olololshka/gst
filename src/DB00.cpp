#include <stdio.h>

#include <qmodbus/ModbusEngine.h>
#include <qmodbus/common.h>
#include <mylog/mylog.h>

#include "DB00.h"

#define SHOW_SCAN_PROGRESS	(1)

DB00::DB00(ModbusEngine* mb, unsigned char adress, QObject* parent) :
    QObject(parent)
{
    ModBus = mb;
    this->adress = adress;
    getDataRequest01 = ModbusRequest::BuildReadInputsRequest(adress,
                                                             _DB00_OUT_DATA_ADRESS,
                                                             2 * sizeof(float) / sizeof(uint16_t)
                                                             );
    getDataRequest34 = ModbusRequest::BuildReadInputsRequest(adress,
                                                             _DB00_OUT_FREQ_ADRESS,
                                                             2 * sizeof(float) / sizeof(uint16_t)
                                                             );
}

DB00::~DB00()
{
    __DELETE(getDataRequest01);
    __DELETE(getDataRequest34);
}

bool DB00::Test()
{
    ModbusRequest* idrequest = ModbusRequest::BuildReadHoldingRequest(adress, _DB00_ID_ADRESS);
    bool res = false;
    ModBus->SyncRequest(*idrequest);
    if (idrequest->getErrorrCode() == ModbusRequest::ERR_OK)
    {
        if (idrequest->getAnsverAsShort().at(0) != _DB00_DEVICE_ID)
            LOG_DEBUG(trUtf8("Found unsupported device ID:%1").arg(idrequest->getAnsverAsShort().at(0), 0, 16));
        else
            res = true;
    }
    __DELETE(idrequest);
    return res;
}

QList<float> DB00::getOutputValues(bool *ok)
{
    ModBus->SyncRequest(*getDataRequest01);
    ModBus->SyncRequest(*getDataRequest34);
    ModbusRequest::ErrorCode err01 = getDataRequest01->getErrorrCode();
    ModbusRequest::ErrorCode err34 = getDataRequest34->getErrorrCode();
    if ((err01 != ModbusRequest::ERR_OK) || (err34 != ModbusRequest::ERR_OK))
    {
        if (ok != NULL)
            *ok = false;
        return QList<float>();
    }
    else
    {
        if (ok != NULL)
            *ok = true;
        QList<float> res(getDataRequest01->getAnsverAsFloat());
        res.append(getDataRequest34->getAnsverAsFloat());
        return res;
    }
}

float DB00::getPreasure(bool *ok)
{
    ModBus->SyncRequest(*getDataRequest01);
    if (getDataRequest01->getErrorrCode() != ModbusRequest::ERR_OK)
    {
        if (ok != NULL)
            *ok = false;
        return 0.0;
    }
    else
    {
        if (ok != NULL)
            *ok = true;
        return getDataRequest01->getAnsverAsFloat().at(0);
    }
}

unsigned char DB00::getAddres() const
{
    return adress;
}
