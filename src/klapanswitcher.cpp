#include <QApplication>

#include <qmodbus/ModbusRequest.h>
#include <qmodbus/ModbusEngine.h>
#include <mylog/mylog.h>

#include <qmodbus/Sleeper.h>

#include "klapanswitcher.h"

KlapanSwitcher::KlapanSwitcher(ModbusEngine *mb, uint8_t addr, QObject *parent) :
    QObject(parent)
{
    this->mb = mb;
    this->addr = addr;

    singleKlapanProtect = false;

    readAllInputsReq = ModbusRequest::BuildReadDiscretInputsRequest(addr, KLAPAN_INPUTS_START_ADRESS, KLAPAN_NINPUTS);
    readAllCoilsReq = ModbusRequest::BuildReadCoilsRequest(addr, KLAPAN_KLAPANS_START_ADRESS, KLAPAN_NKLAPANS);
    writeAcceptBitReq = ModbusRequest::BuildWriteSingleCoilRequest(addr, KLAPAN_ACCEPT_BIT_ADDR, true);
}

KlapanSwitcher::~KlapanSwitcher()
{
    readAllCoilsReq->deleteLater();
    readAllInputsReq->deleteLater();
    writeAcceptBitReq->deleteLater();
}

uint8_t KlapanSwitcher::getAddres() const
{
    return addr;
}

bool KlapanSwitcher::Test() const
{
    ModbusRequest* testreq = ModbusRequest::BuildReadHoldingRequest(addr, 0);
    bool res = false;
    mb->SyncRequest(*testreq);
    if (testreq->getErrorrCode() == ModbusRequest::ERR_OK)
        if (testreq->getAnsverAsShort().at(0) == KLAPAN_CONTROLLER_ID)
            res = true;
    testreq->deleteLater();
    return res;
}

QBitArray KlapanSwitcher::getInputs(bool* ok)
{
    mb->SyncRequest(*readAllInputsReq);
    if (readAllInputsReq->getErrorrCode() != ModbusRequest::ERR_OK)
    {
        if (ok != NULL)
            *ok = false;
        return QBitArray();
    }
    else
    {
        if (ok != NULL)
            *ok = true;
        QBitArray result(KLAPAN_NINPUTS, 0);
        QByteArray resevedVal = readAllInputsReq->getAnsver();
        for (int i = 0; i < KLAPAN_NINPUTS; ++i)
            if (resevedVal.at(i / sizeof(char)) & (1 << (i % sizeof(char))))
                result[i] = true;
        return result;
    }
}

QBitArray KlapanSwitcher::getCoils(bool *ok)
{
    mb->SyncRequest(*readAllCoilsReq);
    if (readAllCoilsReq->getErrorrCode() != ModbusRequest::ERR_OK)
    {
        if (ok != NULL)
            *ok = false;
        return QBitArray();
    }
    else
    {
        if (ok != NULL)
            *ok = true;
        QBitArray result(KLAPAN_NKLAPANS, 0);
        QByteArray resevedVal = readAllCoilsReq->getAnsver();
        for (int i = 0; i < KLAPAN_NKLAPANS; ++i)
            if (resevedVal.at(i / 8) & (1 << (i % 8)))
                result[i] = true;
        return result;
    }
}

bool KlapanSwitcher::switchKlapan(uint16_t number, bool newState)
{
    QByteArray data(number / 8 + 1, 0);
    ModbusRequest* writeCoilRequest;
    if (singleKlapanProtect)
        data[number / 8] = 1 << (number % 8);
    else
    {
        data = KlapanStateForRefresh;
        if (newState)
            data[number / 8] = data[number / 8] | (1 << (number % 8));
        else
            data[number / 8] = data[number / 8] & (~(1 << (number % 8)));
    }
    writeCoilRequest = ModbusRequest::BuildWriteMultiCoilRequest(addr, KLAPAN_KLAPANS_START_ADRESS, data);
    bool res = llRequestCall(writeCoilRequest);
    writeCoilRequest->deleteLater();
    if (res)
        KlapanStateForRefresh = data;
    return res;
}

bool KlapanSwitcher::closeAll()
{
    QByteArray closeallData((KLAPAN_NKLAPANS - 1) / 8  + 1, 0);
    ModbusRequest* closeAllRequest = ModbusRequest::BuildWriteMultiCoilRequest(addr,
                                                                               KLAPAN_KLAPANS_START_ADRESS,
                                                                               closeallData);
    bool res = llRequestCall(closeAllRequest);
    closeAllRequest->deleteLater();
    if (res)
        KlapanStateForRefresh.clear();
    return res;
}

void KlapanSwitcher::setRetryInterval(int msec)
{
    if (timerID != 0)
        killTimer(timerID);
    if (msec >= KLAPAN_MINIMAL_RETRY_INTERVAL)
        startTimer(msec);
}

void KlapanSwitcher::setOnlySingleKlapanProtect(bool on)
{
    singleKlapanProtect = on;
}

bool KlapanSwitcher::acceptNewState()
{
    mb->SyncRequest(*writeAcceptBitReq);
    return writeAcceptBitReq->getErrorrCode() == ModbusRequest::ERR_OK;
}

bool KlapanSwitcher::llRequestCall(ModbusRequest *req)
{
    bool res = true;
    mb->SyncRequest(*req);
    if (req->getErrorrCode() != ModbusRequest::ERR_OK)
        res = false;
    else
        res = acceptNewState();

    return res;
}

void KlapanSwitcher::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);
    if (!KlapanStateForRefresh.isEmpty())
    {
        ModbusRequest* retryReq = ModbusRequest::BuildWriteMultiCoilRequest(addr,
                                                                            KLAPAN_KLAPANS_START_ADRESS,
                                                                            KlapanStateForRefresh);
        int i = 5;
        llRequestCall(retryReq);
        while ((retryReq->getErrorrCode() != ModbusRequest::ERR_OK) && (i--))
        {
            LOG_WARNING("Klapan refresh request failed, retry");
            Sleeper::sleep_ms(10);
            QApplication::processEvents();
            llRequestCall(retryReq);
        }
        retryReq->deleteLater();
    }
}
