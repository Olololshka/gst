Программа управления и автоматизации Генератора Сканирующего Технологического
Исходный код всегда доступен при помощи git

git clone https://bitbucket.org/Olololshka/gst.git

В разработке использованы следующие библиотеки:
+ Qt >=4.6 (used 4.8.4): http://qt-project.org/search/tag/open~source
+ libqserialdevice: http://gitorious.org/qserialdevice
+ fork библиотеки libmodbus: https://github.com/ololoshka2871/libmodbus
+ libqmodbus: https://bitbucket.org/Olololshka/libqmodbus
+ libmylog: https://bitbucket.org/Olololshka/libmylog
+ libsettingscmdline: https://bitbucket.org/Olololshka/libsettingscmdline
+ Qwt: http://qwt.sourceforge.net
+ ALGLIB: http://alglib.sources.ru

+ MinGW: http://www.mingw.org/